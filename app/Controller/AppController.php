<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $helpers = array('Html', 'Form');

	public $components = array(
	    'Auth' => array(
	        'loginAction' => array(
	            'controller' => 'users',
	            'action' => 'login'
	        ),
	        'authError' => 'Did you really think you are allowed to see that?',
	        'authenticate' => array(
	            'Form' => array(
	                'fields' => array(
	                  'username' => 'email', //Default is 'username' in the userModel
	                  'password' => 'password'  //Default is 'password' in the userModel
	                )
	            )
	        )
	    ),'RequestHandler', 'Session'
	);
	public $ImageQuestions="uploads/qs/";
	public $PdfAnswers="uploads/answers/"; 
	public $ImageNews="uploads/news/";

	public $default_limit = 20;
	public $default_limit_dropdown = array("5" => 5, "10" => 10, "25" => 25, "50" => 50, "100" => "All");
	public $boolenStatus = array( "1" => "Active", "0" => "Inactive");
	public $examparts = array("1" => "Part A", "2" => "Part B", "3" => "Part C");

	public function beforeFilter(){
		if (!empty($this->params['prefix']) && $this->params['prefix'] == 'admin') {
	 	$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login','admin'=>true);
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login','admin'=>true);
		$this->Auth->loginRedirect = array('controller' => 'pages', 'action' => 'dashboard','admin'=>true);
		}
		else{
	      $this->Auth->loginAction = array('controller' => 'login','action' => 'index');
	      $this->Auth->logoutRedirect = array('controller' => 'login','action' => 'index');
	    }
		
		$this->set('ImageQuestions',$this->ImageQuestions);
		$this->set('PdfAnswers',$this->PdfAnswers);
		$this->set('ImageNews',$this->ImageNews);
		$this->set('boolenStatus', $this->boolenStatus);
		$this->set('examparts', $this->examparts);

		$this->set('default_limit', $this->default_limit);
		$this->set('default_limit_dropdown', $this->default_limit_dropdown);
	}
	public function imagename($image){
      if($image != ''){
          $newName=rand(0,9999999).'_'.$image;
          $newName= preg_replace('#[ -]+#', '-', preg_replace('/[%()]/', '', preg_replace('/[^a-zA-Z0-9.]+/', ' ', $newName)));
          return $newName;
      }
    }
    public function dateToDb($date = null){
    if(!empty($date)){
            $date = date("Y-m-d", strtotime($date));
            return $date;
        }else{
            return null;
        }
    }

    public function dateFromDb($date = null){
        if(!empty($date)){
            $date = date("d-m-Y", strtotime($date));
            return $date;
        }else{
            return null;
        }
    }
}
