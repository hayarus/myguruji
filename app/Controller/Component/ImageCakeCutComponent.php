<?php
/*!
 * ImageCakeCut => Image Resize & Crop  
 *
 * Author : Sony Thomas K
 * Version 1.4 (for CakePHP)
 * Created : 24/03/2011
 * Date Modified: 31/03/2012
 *
 *	Required Configuration
 *	$this->required_width
 *	$this->required_height
 * 	$this->image_path
 *	$this->save_original
 */
class ImageCakeCutComponent extends Object
{

	//default thumbnail name
	var $thumb_prefix = '';
	//default file size in KB
	var $file_size = 4096;
	//default
	var $no_crop = 0; 
	//default
	var $save_original = 1; 
	
	var $refined_source_width = 0;
	
	//called before Controller::beforeFilter()
	function initialize(&$controller, $settings = array()) {
		// saving the controller reference for later use
		$this->controller =& $controller;
	}
	
	//called after Controller::beforeFilter()
	function startup(&$controller) {
	}
	
	//called after Controller::beforeRender()
	function beforeRender(&$controller) {
	}
	
	//called after Controller::render()
	function shutdown(&$controller) {
	}
	
	//called before Controller::redirect()
	function beforeRedirect(&$controller, $url, $status=null, $exit=true) {
	}

	function setWidth($val)
	{
		$this->required_width =  $val;
	}
	
	function setHeight($val)
	{
		$this->required_height =  $val;
	}
	
	function setImagePath($val)
	{
		$this->image_path =  $val;
		$this->thumb_path =  $val;
	}
	/**
		Always set this value after  setImagePath($val)in controller.
	*/
	function setThumbPath($val)
	{
		$this->thumb_path =  $val;
	}
	function setThumbPrefix($val)
	{
		$this->thumb_prefix =  $val;
	}
	//Set the Maximum file size here - NON FUNCTIONAL
	function setMaxFileSize($val)
	{
		$this->file_size =  $val;
	}
	
	/**
		Determines whether to crop the thumbnail
		@param - $val 
			- 1 => Results in specified $this->required_width and $this->required_height
			- 0 => Uses	$this->required_width as width and calculates height
	*/
	function allowCrop($val)
	{
		$this->no_crop =  $val;
	}
	function refineSourceImage($width)
	{
		$this->refined_source_width =  $width;
	}
	function saveOriginal($val)
	{
		$this->save_original =  $val;
	}
		
	// This function reads the extension of the file.
	// It is used to determine if the file is an image by checking the extension.
	private function getExtension($str) {
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
	
	private function checkParameters($_filesize)
	{
		if(!isset($this->required_width)){
			//print("Error => No width specified.<br/>");
			return $this->getErrorMessages(0);
		}
		elseif(!isset($this->required_height) || !is_int((int)$this->required_height)){
			//print("Error => No height specified.<br/>");
			return $this->getErrorMessages(1);
		}
		elseif(!isset($this->image_path)){
			//print("Error => Image destination not set.<br/>");
			return $this->getErrorMessages(2);
		}
		elseif(!file_exists($this->image_path)){
			//print("Error => Image destination does not exist.<br/>");
			return $this->getErrorMessages(3);
		}
		elseif(!isset($this->thumb_path)){
			//print("Error => Thumbnail destination empty.<br/>");
			return $this->getErrorMessages(4);
		}
		elseif(!file_exists($this->thumb_path)){
			//print("Error => Thumbnail destination does not exist.<br/>");
			return $this->getErrorMessages(5);
		}
		/*elseif(filesize($_filesize)>$this->file_size*1024 && $this->file_size!=0){
			//print("Error => File size exceeds limit.<br/>");
			return $this->getErrorMessages(6);
		}*/
		else{
			return "VALID";
		}
		//if($error_flag == 1)
		//exit;
		
	}
	
	//Displays error messages
	function getErrorMessages($error_code)
	{
		$errArray = array();
		$errArray[0] = "No width specified.";
		$errArray[1] = "No height specified.";
		$errArray[2] = "Image destination not set.";
		$errArray[3] = "Image destination does not exist.";
		$errArray[4] = "Thumbnail destination not set.";
		$errArray[5] = "Thumbnail destination does not exist.";
		$errArray[6] = "File size exceeds limit.";
		
		if($error_code<sizeof($errArray))
			return $errArray[$error_code];
		else
			return "Invalid error code.";
	}
	// this is the function that will create the thumbnail image from the uploaded image
	// the resize will be done considering the width and height defined, but without deforming the image
	function make_thumb($fileTemp,$imageName)
	{
		try{			
			//Check for valid paths,dimension,filesize
			$validity = $this->checkParameters($fileTemp);
			if($validity == "VALID")
			{			
				//get image extension.				
				$ext = strtolower($this->getExtension(stripslashes($imageName)));
	
				//Create a rondom mage name
				//$image_name_template=time().rand(1,999999).'.'.$ext;
				$image_name_template = $imageName;
				$img_name= $this->image_path.$image_name_template;
				//$img_name= $imageName;
				//$original_image_name= $this->image_path.'/original_'.$image_name_template;
				
				//Copy original image to destinationpr($file);exit;				
				//copy($file['tmp_name'][$model]['image'], $img_name);
				
				//ave original copy of image
				if($this->save_original==1){
					copy($fileTemp, $img_name);
				}
							
				//Create a thumb name
				$thumb_name =  $this->thumb_path.$this->thumb_prefix.$image_name_template;
					
				//creates the new image using the appropriate function from gd library
				if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext) || !strcmp("JPG",$ext))
					$src_img=imagecreatefromjpeg($img_name);
				
				if(!strcmp("png",$ext))
					$src_img=imagecreatefrompng($img_name);
				
				if(!strcmp("gif",$ext))
					$src_img=imagecreatefromgif($img_name);				
				
				//Get Original width,height of source image
				$original_w = imageSX($src_img);
				$original_h = imageSY($src_img);				
				
				//Refine uploaded image to reduce size, resolution idf set
				if($this->refined_source_width>0){
					$dst_img=ImageCreateTrueColor($this->refined_source_width,$this->refined_source_width*$original_h/$original_w);
					$refined_source_height = ceil($this->refined_source_width*$original_h/$original_w);	;
					imagecopyresampled($dst_img,$src_img,0,0,0,0,$this->refined_source_width,$refined_source_height,$original_w,$original_h);
					// output the created image to the file. Now we will have the thumbnail into the file named by $filename
					if(!strcmp("png",$ext))
					imagepng($dst_img,$img_name,80);
					if(!strcmp("gif",$ext))
					imagegif($dst_img,$img_name,80);
					else
					imagejpeg($dst_img,$img_name,80);
				}
				
				$calc_height = ceil($this->required_width*$original_h/$original_w);	
				$calc_width = ceil($this->required_height*$original_w/$original_h);
				
				//Set thumb width, height from calculated information.
				if($calc_height<=$this->required_height){
					$thumb_h=$this->required_height;
					$thumb_w=$original_w*$thumb_h/$original_h;
					
				}
				else if($calc_width<=$this->required_width){
					$thumb_w = $this->required_width;
					$thumb_h = $original_h*$thumb_w/$original_w;
				}
			
				// we create a new image with the new dimmensions
				$dst_img=ImageCreateTrueColor($this->required_width,$this->required_height);
				
				// resize the big image to the new created one
				if($this->no_crop==0){
					$dst_img=ImageCreateTrueColor($this->required_width,$calc_height);
					imagecopyresampled($dst_img,$src_img,0,0,0,0,$this->required_width,$calc_height,$original_w,$original_h);
				}
				else if($this->no_crop==1)
					imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$original_w,$original_h);
				
				// output the created image to the file. Now we will have the thumbnail into the file named by $filename
				if(!strcmp("png",$ext))
					imagepng($dst_img,$thumb_name);
				if(!strcmp("gif",$ext))
					imagegif($dst_img,$thumb_name);
				else
					imagejpeg($dst_img,$thumb_name);
				
				//destroys source and destination images.
				imagedestroy($dst_img);
				imagedestroy($src_img);				
			
				return $image_name_template;							
				
			}else{
				return $validity;
			}
			
		}catch(Exception $e){
			print('Error => '.$e);			
		}
	}
	
	
/* ******************** RESIZE FUNCTION ***************** */	


function resize($src, $target, $dim_setting = 'width', $dim1, $dim2 = false)
{
	
	$errors = array();
	list($width, $height, $type, $attr) = getimagesize($src);
						
	list($width, $height) = getimagesize($src);
	$ext = $this->fileExt($target);
						
	// call the proper function based on file extension
	switch ($ext) {
		case 'jpg':
			$src = imagecreatefromjpeg($src);
			break;
		case 'jpeg':
			$src = imagecreatefromjpeg($src);
			break;
		case 'png':
			$src = imagecreatefrompng($src);
			break;
		case 'gif':
			$src = imagecreatefromgif($src);
			break;
		default:
			$errors['ext'] = "Could not recoginze the file extension.";
		}					
							
	// Can we use the GD Library?
	if (!$src) {
		$errors['src'] = "Could not call imagecreatefrom" . $ext . "() in the GD library.";
		return $errors;
	}
							
	// this handles our resize info
	if ($dim_setting == "width") {
		$newWidth = $dim1;
		$newHeight = ($height / $width) * $newWidth;
	} else if ($dim_setting == "height") {
		$newHeight = $dim1;
		$newWidth = ($width / $height) * $newHeight;
	} else {
	// default
	if ($height > $width) {
		$newHeight = $dim1;
		$newWidth = ($width / $height) * $newHeight;
	} else {
		$newWidth = $dim1;
		$newHeight = ($height / $width) * $newWidth;
	}
	}
					
	// create the tmp image and resize it
	$tmp = imagecreatetruecolor($newWidth, $newHeight);
	imagealphablending($tmp, false);
	imagesavealpha($tmp,true);
	$transparent = imagecolorallocatealpha($tmp, 255, 255, 255, 127);
	imagefilledrectangle($tmp, 0, 0, $newWidth, $newHeight, $transparent);
							 
	if (!empty($tmp)) {
		imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
	} else {
		$errors['imagecreatetruecolor'] = "imagecreatetruecolor() failed.";	
	}
							
	/** Create the resized image. **/
	// call the proper function based on file extension
	switch ($ext) {
		case 'jpg':
			imagejpeg($tmp, $target, 100);
			break;
		case 'jpeg':
			imagejpeg($tmp, $target, 100);
			break;
		case 'png':
		{	
			imagefill($tmp, 0, 0, imagecolorallocate($tmp, 255, 255, 255));
			imagealphablending($tmp, TRUE);
			imagepng($tmp, $target, 0);
			break;
		}
		case 'gif':
		{
			imagefill($tmp, 0, 0, imagecolorallocate($tmp, 255, 255, 255));
			imagealphablending($tmp, TRUE);
			imagegif($tmp, $target, 0);
			break;
		}
		default:
			$errors['ext'] = "Could not recoginze the file extension.";
		}	
						
	// destroy the tmp image
	imagedestroy($tmp);
						
	//Return Errors	
	return $errors;
	
}

	
function fileExt ($file) {	
	$ext = substr(strrchr($file, '.'), 1);
	return strtolower($ext);
}
	
	
}
?>