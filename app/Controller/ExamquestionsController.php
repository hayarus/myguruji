<?php
App::uses('AppController', 'Controller');
/**
 * Examquestions Controller
 *
 * @property Examquestion $Examquestion
 * @property PaginatorComponent $Paginator
 */
class ExamquestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();
		// $this->Auth->allow('');
		$this->layout='admin_default';
	}
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Examquestion']['limit'])){
            	$limit = $this->data['Examquestion']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Examquestion.exam_id LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Examquestion.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Examquestion->recursive = 0;
		$examquestions=$this->Examquestion->find('all',array('group'=>'Examquestion.exam_id'));
		$this->set('examquestions',$examquestions, $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Examquestion->exists($id)) {
			throw new NotFoundException(__('Invalid examquestion'));
		}
		$options = array('conditions' => array('Examquestion.' . $this->Examquestion->primaryKey => $id));
		$this->set('examquestion', $this->Examquestion->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Examquestion->create();
			if ($this->Examquestion->save($this->request->data)) {
				$this->Session->setFlash('The examquestion has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The examquestion could not be saved. Please, try again.','flash_failure');
			}
		}
		$exams = $this->Examquestion->Exam->find('list');
		$users = $this->Examquestion->User->find('list');
		$questions = $this->Examquestion->Question->find('list');
		$this->set(compact('exams', 'users', 'questions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Examquestion->exists($id)) {
			throw new NotFoundException(__('Invalid examquestion'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Examquestion->save($this->request->data)) {
				$this->Session->setFlash('The examquestion has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The examquestion could not be saved. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Examquestion.' . $this->Examquestion->primaryKey => $id));
			$this->request->data = $this->Examquestion->find('first', $options);
		}
		$exams = $this->Examquestion->Exam->find('list');
		$users = $this->Examquestion->User->find('list');
		$questions = $this->Examquestion->Question->find('list');
		$this->set(compact('exams', 'users', 'questions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Examquestion->id = $id;
		if (!$this->Examquestion->exists()) {
			throw new NotFoundException(__('Invalid examquestion'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Examquestion->delete()) {
			$this->Session->setFlash('The examquestion has been deleted.','flash_success');
		} else {
			$this->Session->setFlash('The examquestion could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}
	public function admin_examview($id = null) {
		
		$options = array('conditions' => array('Examquestion.exam_id'=> $id));
		$this->set('detail', $this->Examquestion->find('first', $options));
		$examquestions = $this->Examquestion->find('all',array('conditions' => array('Examquestion.exam_id ' => $id)));
		$this->set('examquestions', $examquestions);
	}
}
