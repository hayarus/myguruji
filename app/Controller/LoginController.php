<?php
App::uses('AppController', 'Controller');
class LoginController extends AppController {

public $uses = array('User');
public function beforeFilter() {
		$this->Auth->allow();
	}

public function index(){
		$this->layout = 'frondend';
        $this->loadModel('Exam');
    	$user = $this->Auth->user();
    	if(!empty($user) && $user['group_id']==2 && $user['status']==1){
    		$this->redirect('/');
    	}
    	if ($this->request->is('post')) {

    		if($this->Auth->login()){
    			$user = $this->Auth->user();

    			if($user['group_id']!= 2 ){
    				$this->Auth->logout();
    				$this->Session->setFlash('You do not have permissions to access this section.','flash_failure');
    			}
    			else{
    				
    				if($user['status']!=0 ){
                        $exam_session=$this->Exam->find('all',array('conditions' => array('Exam.user_id' => $user['id'] )));
                        // pr($exam_session);exit;
                        if($exam_session != null){
                        foreach ($exam_session as $key => $exam) {
                            $exam_status=$exam['Exam']['status'];
                            $exam_part=$exam['Exam']['exampart'];
                            $exam_sessionId=$exam['Exam']['id'];

                            // pr($exam_status);exit;
                        }
                         $this->Session->write('exam_session.exam_sessionId', $exam_sessionId);
                         $this->Session->write('exam_session.status', $exam_status);
                         $this->Session->write('exam_session.exampart', $exam_part);

                     }

    				$this->Session->setFlash('You have been logged in succcessfully.','flash_success');
    				$this->redirect(array('controller' => 'pages', 'action' => 'index'));

    				}
    				else{
    					$this->Session->setFlash('You do not have permissions to access this section.','flash_failure');
    				}
    			}
    		}
    		else{
    			$this->Session->setFlash('Invalid email or password. Please try again.','flash_failure');
    		}
    	}
    }

 public function clientlogout() {
		
		
		$this->Session->delete("Auth");
		$this->Session->destroy();
		$this->Session->setFlash('You have logged out successfully.','flash_success');
		$this->redirect(array('action' => 'index'));
	}
}
