<?php
App::uses('AppController', 'Controller');
/**
 * Newses Controller
 *
 * @property Newse $Newse
 * @property PaginatorComponent $Paginator
 */
class NewsesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','ImageCakeCut');

/**
 * index method
 *
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
		// $this->Auth->allow('');
	}
	public function admin_index() {
			if (!empty($this->data)){
			if(isset($this->data['Newse']['limit'])){
            	$limit = $this->data['Newse']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Newse.heading LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Newse.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Newse->recursive = 0;
		$this->set('newses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Newse->exists($id)) {
			throw new NotFoundException(__('Invalid newse'));
		}
		$options = array('conditions' => array('Newse.' . $this->Newse->primaryKey => $id));
		$this->set('newse', $this->Newse->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$image= $this->request->data['Newse']['image']['name'];
			$imageName = 'img-'.rand(0,9999999).'-'.$image;
			if(move_uploaded_file($this->request->data['Newse']['image']['tmp_name'], $this->ImageNews.$imageName)){
					
				$this->ImageCakeCut->resize($this->ImageNews.$imageName, $this->ImageNews.$imageName,'width', 480);
					
			}
			if(!empty($this->request->data['Newse']['dates']))
               $this->request->data['Newse']['dates'] = $this->dateToDb($this->request->data['Newse']['dates']);
			$this->request->data['Newse']['image']= $imageName ;
			$this->Newse->create();
			if ($this->Newse->save($this->request->data)) {
				$this->Session->setFlash('The newse has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The newse could not be saved. Please, try again.','flash_failure');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$ImageNews=$this->ImageNews;
		if (!$this->Newse->exists($id)) {
			throw new NotFoundException(__('Invalid newse'));
		}

		if(empty($this->request->data['Newse']['image']['name']))
				unset($this->request->data['Newse']['image']);
			else {
				$crntImgdetails = $this->Newse->find('first',array('conditions'=>array('Newse.id'=>$id)));
				$crntImg = $crntImgdetails['Newse']['image'];
				@unlink($ImageNews.$crntImg);
				
				$image= $this->request->data['Newse']['image']['name'];
				$imageName = 'img-'.rand(0,9999999).'-'.$image;
				
				if(move_uploaded_file($this->request->data['Newse']['image']['tmp_name'], $ImageNews.$imageName)){
				$this->ImageCakeCut->resize($ImageNews.$imageName, $ImageNews.$imageName,'width', 480); }
				$this->request->data['Newse']['image']= $imageName ;
			}
			if ($this->request->is(array('post', 'put'))) {

			if(!empty($this->request->data['Newse']['dates']))
                $this->request->data['Newse']['dates'] = $this->dateToDb($this->request->data['Newse']['dates']);
			if ($this->Newse->save($this->request->data)) {
				$this->Session->setFlash('The newse has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The newse could not be saved. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Newse.' . $this->Newse->primaryKey => $id));
			$this->request->data = $this->Newse->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$ImageNews= $this->ImageNews;

		$this->Newse->id = $id;
		if (!$this->Newse->exists()) {
			throw new NotFoundException(__('Invalid newse'));
		}
          $options= array('conditions'=>array('Newse.id'=>$id));
			$arrImage= $this->Newse->find('first',$options);
			
			$fileLarge = new File($ImageNews.$arrImage['Newse']['image']);
			
			$fileLarge->delete();
			


		$this->request->allowMethod('post', 'delete');
		if ($this->Newse->delete()) {
			$this->Session->setFlash('The newse has been deleted.','flash_success');
		} else {
			$this->Session->setFlash('The newse could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}
}

