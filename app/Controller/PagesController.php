<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='frondend';
		$this->Auth->allow('index','aboutus','contactus','questions','howtoapply','registration','news','exampart');
	}
public function admin_dashboard() {
		$this->layout='admin_default';
	}

public function index() {
	$this->layout='homelayout';
	$this->loadmodel('Newse');
	$allnews = $this->Newse->find('all',array('order'=>'Newse.id','conditions'=>array('Newse.status=1')));
	$this->set('allnews', $allnews);
		
	}
	public function aboutus() {

	}

	public function news($id = null) {
		
	$this->loadModel('Newse');
	if (!$this->Newse->exists($id)) {
			throw new NotFoundException(__('Invalid newse'));
		}
		$options = array('conditions' => array('Newse.' . $this->Newse->primaryKey => $id));
		$this->set('news', $this->Newse->find('first', $options));

		$this->loadmodel('Newse');
		$allnews = $this->Newse->find('all',array('conditions'=>array('Newse.status=1')));
		$this->set('allnews', $allnews);
	}

	public function contactus(){
	if($this->request->is('post')){
	    $name=$this->request->data['name'];
	    $email= $this->request->data['emailid'];
	    $subject = "Contact";
	    $message= $this->request->data['message'];
	    $admin_email='jishnu@hayarus.com';
	    
        $fromemail=$email;
	    $to = $admin_email;
	   
		$txt = '<html><body>';
		$txt .="
		Hi Admin,
		<br/><br/>

	   	<table> 
	    <tr><td>Name: $name</td></tr>
		<tr><td>Email     : $email</td></tr>
		<tr><td>Message   : $message</td></tr>
	    </table>
		</br></br><br/><br style='clear:both'/>
	    Best Regards,<br/>".$name . "
	    ";
		$txt .= "</body></html>";
        
		$headers = "From: ".$name." <" . $fromemail. ">\r\n";
		$headers .= "Reply-To: ".$fromemail."\r\n";
		
		//$headers .='X-Mailer: PHP/' . phpversion();
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 		
		if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
		{
		    $this->Session->setFlash('Thank you for contacting us. The enquiry has been received. We will get back you soon.','success');
				return $this->redirect(array('controller'=>'pages','action' => 'index'));
		}else{
		    $this->Session->setFlash('Error while sending mail.','fail_flash');
	         return $this->redirect(array('controller'=>'pages','action' => 'index'));
		}

	}
	
}
public function questions(){
	$this->loadmodel('Questionpaper');
	$questionpapers = $this->Questionpaper->find('all');
	$this->set('questionpapers', $questionpapers);
	
}

public function howtoapply(){
	
}

public function registration(){
	$this->loadModel('User');
	if($this->request->is('post')){
		$this->request->data['User']['group_id'] = 2;
		$this->request->data['User']['status'] = 1;
	$this->User->create();
	if ($this->User->save($this->request->data)) {
	// $this->Session->setFlash('The user has been saved.','flash_success');
	return $this->redirect(array('controller'=>'pages','action' => 'index'));
	} else {
	$this->Session->setFlash('The user could not be saved. Please, try again.','flash_failure');
	return $this->redirect(array('controller'=>'pages','action' => 'index'));
	}

	}
	
}
public function exampart(){
	$id   = isset($this->request->query['id'])   ? $this->request->query['id']   : null;
   	$part  = isset($this->request->query['part'])  ? $this->request->query['part']  : null;
   	$this->loadModel('Exam');
   	
   	$this->loadModel('Examquestion');
   	if($this->request->is('post')){
   	$this->request->data['Exam']['user_id']=$id;
   	$this->request->data['Exam']['date']=date("Y-m-d");
   	$this->request->data['Exam']['exampart']=$part;
   	$this->request->data['Exam']['status']=1;
   	// pr($this->request->data);exit;
   	$this->Exam->Create();
   	if ($reg=$this->Exam->save($this->request->data)) {
	// $this->Session->setFlash('The user has been saved.','flash_success');
   		$exam_id=$this->Exam->id;
   		$this->Session->write('exam_session.exam_sessionId', $exam_id);
   		$this->Session->write('exam_session.status', 1);
   		$this->Session->write('exam_session.exampart', $part);

   		 if(!empty($reg)) {
			$this->loadModel('Question');
			if($part==1){
   			$questions=$this->Question->find('all',array( 'conditions' => array('Question.exampart' => $part), 'order' => 'rand()','limit' => 25));
   			}elseif($part==2){
   			$questions=$this->Question->find('all',array( 'conditions' => array('Question.exampart' => $part), 'order' => 'rand()','limit' => 35));
   			}elseif($part==3){
   			$questions=$this->Question->find('all',array( 'conditions' => array('Question.exampart' => $part), 'order' => 'rand()','limit' => 40));
   			}

		// pr($questions);exit;
   		foreach ($questions as $key => $question) { 
   			// pr($question['Question']['id']);
   			$examquestions['Examquestion']['question_id'] = $question['Question']['id'];
   			$examquestions['Examquestion']['exam_id'] = $exam_id;
   			$examquestions['Examquestion']['user_id'] = $id;
   			$this->Examquestion->create();
   			$this->Examquestion->save($examquestions);
   			}

   		}
	return $this->redirect(array('controller'=>'pages','action' => 'test'));
	} else {
	// $this->Session->setFlash('The user could not be saved. Please, try again.','flash_failure');
	return $this->redirect(array('controller'=>'pages','action' => 'index'));
	}
	}
}
public function test(){
	$this->loadModel('Examquestion');
	$this->loadModel('Question');
	$this->loadModel('Exam');
	 $user = $this->Session->read('Auth.User');
	 // Pr($user['id']);exit;
	 $examquestions=$this->Examquestion->find('all',array('conditions' => array('Examquestion.user_id' => $user['id'] , 'Examquestion.status' =>0),'limit' => 1));
	 // pr($examquestions);exit;
	  if($examquestions == null){

	  	// return $this->redirect(array('action' => 'index'));
	  	$exam_session = $this->Session->read('exam_session');
	  	$id=$exam_session['exam_sessionId'];
	  	$this->redirect(array('action' => 'examstatus', '?' => array('id' => $id )));
	  
	  	 }
	 else{

	 foreach ($examquestions as $key => $examquestion) { 
		// Pr($examquestion['Examquestion']['question_id']);exit;
		$question_id=$examquestion['Examquestion']['question_id'];	
		$id=$examquestion['Examquestion']['id'];	
		// pr($id);exit;
	}
	// pr($question_id);exit;
	
	$questions = $this->Question->find('all', array('conditions' => array('Question.id' => $question_id)));
	// Pr($question['Question']['question']);exit;
	foreach ($questions as $key => $question) { 
	$answer=$question['Question']['answer'];
	// pr($answer);exit;
	}

	if ($this->request->is(array('post', 'put'))) {
		if($this->request->data['Examquestion']['answer']==null)
		{
		$this->request->data['Examquestion']['status']=1;
		}else {
		$this->request->data['Examquestion']['status']=2;
		// pr($this->request->data['Examquestion']['answer']);exit;
		}
		if($answer==$this->request->data['Examquestion']['answer']){
		   $this->request->data['Examquestion']['mark']=2;
		} 
		else {
			$this->request->data['Examquestion']['mark']=0;
		}
		if ($this->Examquestion->save($this->request->data)) {

			return $this->redirect(array('action' => 'test'));
			// pr($examquestions);exit;
		}

		} 
		else {
			$options = array('conditions' => array('Examquestion.' . $this->Examquestion->primaryKey => $id));
			$this->request->data = $this->Examquestion->find('first', $options);
		}
	$this->set('questions',$questions);	
}


}
 public function examresult(){
	$this->loadModel('Exam');
	$this->loadModel('Examquestion');
	$this->loadModel('Question');
	$user = $this->Session->read('Auth.User');
	$exam_session = $this->Session->read('exam_session');
	//pr($exam_session['exam_sessionId']);
	$examsheets=$this->Examquestion->find('all',array('conditions' => array('Examquestion.exam_id' =>$exam_session['exam_sessionId'] ,'Examquestion.user_id' => $user['id'] )));
	$questions = $this->Examquestion->Question->find('list');
	$exammarks=$this->Examquestion->find('all',array('conditions' => array('Examquestion.exam_id' =>$exam_session['exam_sessionId'] ,'Examquestion.user_id' => $user['id'] , 'Examquestion.mark' => 2 )));
	$mark=count($exammarks) * 2; 
	// pr($mark);exit;
	$this->set(compact('examsheets','questions','mark'));
 }


 public function examstatus(){
 	$this->loadModel('Exam');
		$id=isset($this->request->query['id'])   ? $this->request->query['id']   : null;
		$no=isset($this->request->query['no'])   ? $this->request->query['no']   : null;
		// pr($id);exit;
		if (!$this->Exam->exists($id)) {
			throw new NotFoundException(__('Invalid exam'));
		}
		if ($this->request->is(array('post', 'put'))) {
              $this->request->data['Exam']['status'] = 2;
			if ($this->Exam->save($this->request->data)) {
			// 	$this->Session->setFlash('The exam has been saved.','flash_success');
				if($no == 1){
					return $this->redirect(array('action' => 'index'));
				}
				else{
				$this->Session->write('exam_session.exam_sessionId',$id);
				return $this->redirect(array('action' => 'examresult'));
				}
			} 
			else {
				$this->Session->setFlash('The exam could not be saved. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Exam.' . $this->Exam->primaryKey => $id));
			$this->request->data = $this->Exam->find('first', $options);
		}

	}
}
