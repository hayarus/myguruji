<?php
App::uses('AppController', 'Controller');
/**
 * Questionpapers Controller
 *
 * @property Questionpaper $Questionpaper
 * @property PaginatorComponent $Paginator
 */
class QuestionpapersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
	public function admin_index() {
		$this->Questionpaper->recursive = 0;
		$this->set('questionpapers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Questionpaper->exists($id)) {
			throw new NotFoundException(__('Invalid questionpaper'));
		}
		$options = array('conditions' => array('Questionpaper.' . $this->Questionpaper->primaryKey => $id));
		$this->set('questionpaper', $this->Questionpaper->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$files= $this->request->data['Questionpaper']['question']['name'];
			$fileName = 'que-'.rand(0,9999999).'-'.$files;
			
			$anskeys=$this->request->data['Questionpaper']['answerkey']['name'];
			$anskeyName = 'ans-'.rand(0,9999999).'-'.$anskeys;

			move_uploaded_file($this->request->data['Questionpaper']['question']['tmp_name'], $this->ImageQuestions.$fileName);
			move_uploaded_file($this->request->data['Questionpaper']['answerkey']['tmp_name'], $this->PdfAnswers.$anskeyName);
			$this->request->data['Questionpaper']['question']= $fileName ;
			$this->request->data['Questionpaper']['answerkey']= $anskeyName ;




			$this->Questionpaper->create();
			if ($this->Questionpaper->save($this->request->data)) {
				$this->Session->setFlash('The questionpaper has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The questionpaper could not be saved. Please, try again.','flash_failure');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	$ImageQuestions=$this->ImageQuestions;
	$PdfAnswers=$this->PdfAnswers;

		if (!$this->Questionpaper->exists($id)) {
			throw new NotFoundException(__('Invalid questionpaper'));
		}

		if ($this->request->is(array('post', 'put'))) {



			if(empty($this->request->data['Questionpaper']['question']['name']))
				unset($this->request->data['Questionpaper']['question']);
			else {
				$crntQuesdetails = $this->Questionpaper->find('first',array('conditions'=>array('Questionpaper.id'=>$id)));
				$crntQues= $crntQuesdetails['Questionpaper']['question'];
				@unlink($ImageQuestions.$crntImg);
				
				
				$files= $this->request->data['Questionpaper']['question']['name'];
				$fileName = 'que-'.rand(0,9999999).'-'.$files;
				
				move_uploaded_file($this->request->data['Questionpaper']['question']['tmp_name'], $ImageQuestions.$fileName);
						
				$this->request->data['Questionpaper']['question']= $fileName ;
			}
			
			if(empty($this->request->data['Questionpaper']['answerkey']['name']))
				unset($this->request->data['Questionpaper']['answerkey']);
			else {
				$crntAnsdetails = $this->Questionpaper->find('first',array('conditions'=>array('Questionpaper.id'=>$id)));
				$crntAns = $crntAnsdetails['Questionpaper']['answerkey'];
				@unlink($PdfAnswers.$crntAns);
				
				
				$anskeys=$this->request->data['Questionpaper']['answerkey']['name'];
				$anskeyName = 'ans-'.rand(0,9999999).'-'.$anskeys;
				
				move_uploaded_file($this->request->data['Questionpaper']['answerkey']['tmp_name'], $PdfAnswers.$anskeyName);
						
				$this->request->data['Questionpaper']['answerkey']= $anskeyName ;
			}
			

			if ($this->Questionpaper->save($this->request->data)) {
				$this->Session->setFlash('The Questionpaper has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The questionpaper could not be saved. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Questionpaper.' . $this->Questionpaper->primaryKey => $id));
			$this->request->data = $this->Questionpaper->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$ImageQuestions= $this->ImageQuestions;
		$PdfAnswers=$this->PdfAnswers;
		$this->Questionpaper->id = $id;
		if (!$this->Questionpaper->exists()) {
			throw new NotFoundException(__('Invalid questionpaper'));
		}
		$options= array('conditions'=>array('Questionpaper.id'=>$id));
			$arrQues= $this->Questionpaper->find('first',$options);
			
			$fileQues = new File($ImageQuestions.$arrQues['Questionpaper']['question']);
			$fileAns = new File($PdfAnswers.$arrQues['Questionpaper']['answerkey']);
			$fileQues->delete();
			$fileAns->delete();
			
		$this->request->allowMethod('post', 'delete');
		if ($this->Questionpaper->delete()) {
			$this->Session->setFlash('The questionpaper has been deleted.','flash_success');
		} else {
			$this->Session->setFlash('The questionpaper could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
