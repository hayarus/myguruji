<?php
App::uses('AppController', 'Controller');
/**
 * Results Controller
 *
 * @property Result $Result
 * @property PaginatorComponent $Paginator
 */
class ResultsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function beforeFilter(){
		parent::beforeFilter();
		$this->layout='admin_default';
	}
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Result']['limit'])){
            	$limit = $this->data['Result']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Result.exam_id LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Result.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Result->recursive = 0;
		$this->set('results', $this->Paginator->paginate());
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Result->exists($id)) {
			throw new NotFoundException(__('Invalid result'));
		}
		$options = array('conditions' => array('Result.' . $this->Result->primaryKey => $id));
		$this->set('result', $this->Result->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Result->create();
			if ($this->Result->save($this->request->data)) {
				$this->Session->setFlash('The result has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The result could not be saved. Please, try again.','flash_failure');
			}
		}
		$exams = $this->Result->Exam->find('list');
		$users = $this->Result->User->find('list');
		$this->set(compact('exams', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Result->exists($id)) {
			throw new NotFoundException(__('Invalid result'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Result->save($this->request->data)) {
				$this->Session->setFlash('The result has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The result could not be saved. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Result.' . $this->Result->primaryKey => $id));
			$this->request->data = $this->Result->find('first', $options);
		}
		$exams = $this->Result->Exam->find('list');
		$users = $this->Result->User->find('list');
		$this->set(compact('exams', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Result->id = $id;
		if (!$this->Result->exists()) {
			throw new NotFoundException(__('Invalid result'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Result->delete()) {
			$this->Session->setFlash('The result has been deleted.','flash_success');
		} else {
			$this->Session->setFlash('The result could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
