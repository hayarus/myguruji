<?php
App::uses('AppController', 'Controller');
/**
 * Subscriptions Controller
 *
 * @property Subscription $Subscription
 * @property PaginatorComponent $Paginator
 */
class SubscriptionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
		// $this->Auth->allow('');
	}
	public function admin_index() {

		if (!empty($this->data)){
			if(isset($this->data['Subscription']['limit'])){
            	$limit = $this->data['Subscription']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Subscription.details LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Subscription.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Subscription->recursive = 0;
		$this->set('subscriptions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Subscription->exists($id)) {
			throw new NotFoundException(__('Invalid subscription'));
		}
		$options = array('conditions' => array('Subscription.' . $this->Subscription->primaryKey => $id));
		$this->set('subscription', $this->Subscription->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Subscription->create();
			if ($this->Subscription->save($this->request->data)) {
				$this->Session->setFlash('The subscription has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The subscription could not be saved. Please, try again.','flash_failure');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Subscription->exists($id)) {
			throw new NotFoundException(__('Invalid subscription'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Subscription->save($this->request->data)) {
				$this->Session->setFlash('The subscription has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The subscription could not be saved. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Subscription.' . $this->Subscription->primaryKey => $id));
			$this->request->data = $this->Subscription->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Subscription->id = $id;
		if (!$this->Subscription->exists()) {
			throw new NotFoundException(__('Invalid subscription'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Subscription->delete()) {
			$this->Session->setFlash('The subscription has been deleted.','flash_success');
		} else {
			$this->Session->setFlash('The subscription could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}
}
