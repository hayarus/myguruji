<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
public function beforeFilter() {
	parent::beforeFilter();
	$this->layout='admin_default';
	$this->Auth->allow('admin_add');
}
	
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['User']['limit'])){
            	$limit = $this->data['User']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'User.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'User.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}
	public function admin_subscriptionindex() {

			if (!empty($this->data)){
			if(isset($this->data['User']['limit'])){
            	$limit = $this->data['User']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'User.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'User.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->User->recursive = 0;
		$users=$this->User->find('all',array('conditions'=>array('User.subscription_id !='=>0)));
		$this->set('users',$users, $this->Paginator->paginate());
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('The user has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The user could not be saved. Please, try again.','flash_failure');
			}
		}
		$groups = $this->User->Group->find('list');
		$subscriptions = $this->User->Subscription->find('list');
		$this->set(compact('groups', 'subscriptions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('The user has been saved.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The user could not be saved. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$groups = $this->User->Group->find('list');
		$subscriptions = $this->User->Subscription->find('list');
		$this->set(compact('groups', 'subscriptions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash('The user has been deleted.','flash_success');	
			} else {
				$this->Session->setFlash('The user could not be deleted. Please, try again.','flash_failure');
			}
		return $this->redirect(array('action' => 'index'));
	}
public function admin_login(){
		
			$this->layout="admin_login";
			if($this->request->is('post')){//pr($this->request->data);exit;
				$this->request->data['User']['email']=$this->request->data['User']['email'];
				$this->request->data['User']['password']=$this->request->data['User']['password'];
				$this->Session->delete('Auth');
				if($this->Auth->login()){
					//echo "here";exit;
					$this->Session->delete('sessionUserInfo');
					$userInfo = $this->Auth->user();
					$this->User->id = $this->Auth->user('id');
					$this->redirect($this->Auth->redirectUrl());
					$this->Session->write("sessionUserInfo",$userInfo);
					
					
				}else{
					$this->Session->setFlash('Incorrect email or password, try again.', 'flash_failure');
					return $this->redirect($this->Auth->redirect());
				}
				//pr($this->request->data);exit;
			}	
			
		}

	public function admin_logout(){
		$this->Session->delete("sessionUserInfo");
		$this->Session->delete("Auth");
		$this->Session->destroy();
		$this->Session->setFlash('You have successfully logged out.', 'flash_success');
		//pr($this->Session->read());exit;
		$this->redirect($this->Auth->logout());
		
	}
	}
