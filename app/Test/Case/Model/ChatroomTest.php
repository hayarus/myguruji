<?php
App::uses('Chatroom', 'Model');

/**
 * Chatroom Test Case
 */
class ChatroomTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.chatroom',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Chatroom = ClassRegistry::init('Chatroom');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Chatroom);

		parent::tearDown();
	}

}
