<?php
App::uses('Questionpaper', 'Model');

/**
 * Questionpaper Test Case
 */
class QuestionpaperTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.questionpaper'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Questionpaper = ClassRegistry::init('Questionpaper');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Questionpaper);

		parent::tearDown();
	}

}
