<?php $user = $this->Session->read('Auth.User'); 
      $this->set('tabPermission', $this->Session->read("tabPermission"));
?>
<ul class="page-sidebar-menu">
<li class="sidebar-toggler-wrapper">
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <div class="sidebar-toggler hidden-phone">
                    </div>
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                </li>
                <br>
                
                <li class=" <?php if($this->request->params['controller']=='dashboard' && ($this->request->params['action']=='index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/pages/dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">
                        Dashboard
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if($this->request->params['controller']=='groups' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/groups/index">
                    <i class="fa fa-group"></i>
                    <span class="title">
                        Groups
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if($this->request->params['controller']=='questionpapers' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/questionpapers/index">
                    <i class="fa fa-question"></i>
                    <span class="title">
                        Question Papers
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if($this->request->params['controller']=='questions' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/questions/index">
                    <i class="fa fa-gears"></i>
                    <span class="title">
                        Questions
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if($this->request->params['controller']=='newses' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/newses/index">
                    <i class="fa fa-tablet"></i>
                    <span class="title">
                       News
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if($this->request->params['controller']=='subscriptions' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/subscriptions/index">
                    <i class="fa fa-book"></i>
                    <span class="title">
                        Subscriptions
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if($this->request->params['controller']=='exams' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/exams/index">
                    <i class="fa fa-tasks"></i>
                    <span class="title">
                        Exam Registrations
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if($this->request->params['controller']=='examquestions' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' ||  $this->request->params['action'] == 'admin_examview' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/examquestions/index">
                    <i class="fa fa-clipboard"></i>
                    <span class="title">
                        Exam Sheets
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>

                <li class=" <?php if($this->request->params['controller']=='results' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/results/index">
                    <i class="fa fa-list"></i>
                    <span class="title">
                        Exam Results
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                  <li class=" <?php if($this->request->params['controller']=='users' && ( $this->request->params['action'] == 'admin_subscriptionindex'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/users/subscriptionindex">
                    <i class="fa fa-adjust"></i>
                    <span class="title">
                        Subscribed Users
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class="" >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/users/logout">
                    <i class="fa fa-power-off"></i>
                    <span class="title">
                        Logout  
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                
</ul>                
<!-- <li class=" <?php if($this->request->params['controller']=='users' && ($this->request->params['action']=='add' || $this->request->params['action'] == 'edit' || $this->request->params['action'] == 'view' || $this->request->params['action'] == 'subscriptionindex'))echo 'active';?>" >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/users/subscriptionindex">
                    <i class="fa fa-book"></i>
                    <span class="title">
                       Subscribed Users
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li> -->