
<?php 
 $user = $this->Session->read('Auth.User'); 
 $exam_session = $this->Session->read('exam_session');
 ?>
<nav class="navbar navbar-default">
<div class="container-fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Menu</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<!-- <li><a href="<?php echo $this->webroot;?>">HOME</a></li> -->
<li>
<a href="<?php echo $this->webroot;?>">HOME</a>
</li>
<li><a href="<?php echo $this->webroot;?>pages/aboutus">ABOUT</a></li>
<li><a href="<?php echo $this->webroot;?>pages/contactus">CONTACT</a></li>
<li><a href="<?php echo $this->webroot;?>pages/questions">QUESTIONS</a></li>
<li><a href="<?php echo $this->webroot;?>pages/howtoapply">HOW TO APPLY</a></li>
<?php

 if(!empty($user) && $user['group_id']==2 && $user['status']==1 && $exam_session['status']==0 ){?>
<li><a href="<?php echo $this->webroot;?>pages/exampart" >TEST</a></li>
<?php } elseif(!empty($user) && $user['group_id']==2 && $user['status']==1 && $exam_session['status']==1 ) { 

	?>
<li><a href="<?php echo $this->webroot;?>pages/test" >TEST</a></li>
<?php } elseif(!empty($user) && $user['group_id']==2 && $user['status']==1 && $exam_session['status']==2 ) { ?>
<li><a href="<?php echo $this->webroot;?>pages/exampart" >TEST</a></li>
<?php } ?> 	

 <?php if(!empty($user) && $user['group_id']==2 && $user['status']==1){?>
<li><a href="<?php echo $this->webroot;?>login/clientlogout" > LOGOUT</a></li>
<?php }?>
<!-- <li><a href="#video">VIDEO</a></li>
<li><a href="#students">STUDENTS</a></li>
<li><a href="#contact">CONTACT</a></li> -->
</ul>
</div>
</div>
</nav>

