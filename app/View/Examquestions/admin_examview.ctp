<!-- <div class="examquestions view">
<h2><?php echo __('Examquestion'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($examquestion['Examquestion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exam'); ?></dt>
		<dd>
			<?php echo $this->Html->link($examquestion['Exam']['id'], array('controller' => 'exams', 'action' => 'view', $examquestion['Exam']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($examquestion['Question']['id'], array('controller' => 'questions', 'action' => 'view', $examquestion['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answer'); ?></dt>
		<dd>
			<?php echo h($examquestion['Examquestion']['answer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mark'); ?></dt>
		<dd>
			<?php echo h($examquestion['Examquestion']['mark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($examquestion['Examquestion']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($examquestion['Examquestion']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($examquestion['Examquestion']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Examquestion'), array('action' => 'edit', $examquestion['Examquestion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Examquestion'), array('action' => 'delete', $examquestion['Examquestion']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $examquestion['Examquestion']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Examquestions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Examquestion'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Exams'), array('controller' => 'exams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Exam'), array('controller' => 'exams', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
 -->

 <?php $this->Html->addCrumb('Exam Sheets', '/admin/examquestions'); ?>
<?php $this->Html->addCrumb('Exam Details', ''); ?>
<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box green">
				<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i><?php echo __('Exam Details'); ?>                       
				</div>
				<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				</div>
				</div>
					<div class="portlet-body form users">
						
						<!-- BEGIN FORM-->
						<?php echo $this->Form->create('', array('class' => 'form-horizontal' ,'method'=>'post')); ?>
						<div class="form-body">  
							<div class="form-group">
								<label class="col-md-3 control-label" for="">&nbsp;</label>
								<div class="col-md-4">
									<div class="input text">
										<!-- <span class="required" style="color:#F00"> *</span>= Required -->
									</div>
								</div>
							</div>                                
							<div class="form-group">
								<!--<?php echo $this->Form->label('id<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?> -->
								<div class='col-md-4'>
									<?php echo $this->Form->input('id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('Name Of Applicant<span class=required> </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->label($detail['User']['name'] ,null, array('class' => 'col-md-3 control-label'));  ?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('registration no<span class=required>  </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->label($detail['Exam']['id'] ,null, array('class' => 'col-md-3 control-label'));  ?>
								</div>
							</div>
							
							<div class="form-group">
								<?php echo $this->Form->label('<span class=required> </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-9'>
									<?php echo $this->Form->label('' ,null, array('class' => 'col-md-3 control-label')); ?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('<span class=required> </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-9'>
									<!-- <?php echo $this->Form->label('' ,null, array('class' => 'col-md-3 control-label')); ?> -->
									<h3 class = "col-md-3 control-label">Answer Sheet</h3>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('<span class=required></span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-8'>
								<table class="table table-border">
									<tr>
										<th>Question</th><th>Answer</th><th>Mark</th>
									</tr>
									<?php foreach ($examquestions as $key => $examquestion){?>
									<tr>
									
									<td>
										<?php echo $examquestion['Question']['question']; ?>
									</td>
									<td>
										<?php echo $examquestion['Examquestion']['answer']; ?>
									</td>
									<td><?php echo $examquestion['Examquestion']['mark']; ?>&nbsp;</td>
																</tr>
									<?php }?>
								</table>
							   </div>
							</div>
							<!-- <div class="form-group">
								<?php echo $this->Form->label('question_id<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('question_id', array('class' => 'form-control', 'label' => false, 'required' => true));?>
								</div>
							</div>


							<div class="form-group">
								<?php echo $this->Form->label('tastefield_id<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('tastefield_id', array('class' => 'form-control', 'label' => false, 'required' => true));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('mark<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('mark', array('class' => 'form-control', 'label' => false, 'required' => true));?>
								</div>
							</div>

								<div class="form-group">
								<?php echo $this->Form->label('status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => true));?>
								</div>
							</div>

							     
							
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/Exams'; ?>'">Cancel</button>
								</div>
							</div>
						</div> -->
						<?php echo $this->Form->end(); ?>
						<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>