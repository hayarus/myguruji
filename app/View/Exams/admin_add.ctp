<!-- <div class="exams form">
<?php echo $this->Form->create('Exam'); ?>
	<fieldset>
		<legend><?php echo __('Add Exam'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('date');
		echo $this->Form->input('exampart');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Exams'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Examquestions'), array('controller' => 'examquestions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Examquestion'), array('controller' => 'examquestions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Results'), array('controller' => 'results', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Result'), array('controller' => 'results', 'action' => 'add')); ?> </li>
	</ul>
</div>
 -->

  <?php $this->Html->addCrumb('Exams', '/admin/exams'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<script type="text/javascript">

$(document).ready(function(){
var error1 = $('.alert-danger');
$('#ExamAdminAddForm').validate({
errorElement: 'span', //default input error message container
errorClass: 'help-block', // default input error message classfalse
focusInvalid: false, // do not focus the last invalid input
ignore: "",
rules:{
"data[Exam][user_id]" : {required : true },
"data[Exam][date]" : {required : true},
"data[Exam][exampart]" : { required : true},
"data[Exam][status]" : {required : true},
},
messages:{
"data[Exam][user_id]" : {required :"Please enter heading."},
"data[Exam][date]" : {required :"Please select  date."},
"data[Exam][exampart]" : {required :"Please select exam part."},
"data[Exam][status]" : { accept :"Please select status"}
},

invalidHandler: function (event, validator) { //display error alert on form submit              
//success1.hide();
error1.show();
//App.scrollTo(error1, -200);
},

highlight: function (element) { // hightlight error inputs
$(element)
.closest('.form-group').addClass('has-error'); // set error class to the control group
},

unhighlight: function (element) { // revert the change done by hightlight
$(element)
.closest('.form-group').removeClass('has-error'); // set error class to the control group
},

success: function (label) {
label
.closest('.form-group').removeClass('has-error'); // set success class to the control group
label
.closest('.form-group').removeClass('error');
},
});
});

</script>

<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i><?php echo __('Add Exam'); ?>                        
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body form users">
						<div class="alert alert-danger display-hide">
						<button data-close="alert" class="close"></button>
						You have some form errors. Please check below.
						</div>

						<?php echo $this->Form->create('Exam', array('class' => 'form-horizontal','method'=>'post')); ?>

						<div class="form-body">  
							<div class="form-group">
								<label class="col-md-3 control-label" for="">&nbsp;</label>
								<div class="col-md-4">
									<div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
								</div>
							</div>   
							<div class="form-group">
								<?php echo $this->Form->label('Name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('user_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<div class="form-group">
							<?php echo $this->Form->label('date<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
						 	<div class='col-md-4'>	
									<div class="input-group date date-picker" data-date-format="dd-mm-yyyy" data-link-field="CallDateOpened">
										<input type="text" size="16"  class="form-control" name="data[Exam][date]"  readonly>
										<span class="input-group-btn">
										<button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>											
						    </div>
						    </div>
							<div class="form-group">
								<?php echo $this->Form->label('exam part<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('exampart', array('class' => 'form-control', 'label' => false, 'required' => false,'options'=>$examparts));?>
								</div>
							</div> 
							<div class="form-group">
								<?php echo $this->Form->label('status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => false,'options'=>$boolenStatus));?>
								</div>
							</div> 
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/exams'; ?>'">Cancel</button>
								</div>
							</div>
						</div>
						
						<?php echo $this->Form->end(); ?>
					<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
