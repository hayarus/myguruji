<div class="exams view">
<h2><?php echo __('Exam'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($exam['Exam']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($exam['User']['name'], array('controller' => 'users', 'action' => 'view', $exam['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($exam['Exam']['date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exampart'); ?></dt>
		<dd>
			<?php echo h($exam['Exam']['exampart']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($exam['Exam']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($exam['Exam']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($exam['Exam']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Exam'), array('action' => 'edit', $exam['Exam']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Exam'), array('action' => 'delete', $exam['Exam']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $exam['Exam']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Exams'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Exam'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Examquestions'), array('controller' => 'examquestions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Examquestion'), array('controller' => 'examquestions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Results'), array('controller' => 'results', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Result'), array('controller' => 'results', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Examquestions'); ?></h3>
	<?php if (!empty($exam['Examquestion'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Exam Id'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th><?php echo __('Answer'); ?></th>
		<th><?php echo __('Mark'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($exam['Examquestion'] as $examquestion): ?>
		<tr>
			<td><?php echo $examquestion['id']; ?></td>
			<td><?php echo $examquestion['exam_id']; ?></td>
			<td><?php echo $examquestion['question_id']; ?></td>
			<td><?php echo $examquestion['answer']; ?></td>
			<td><?php echo $examquestion['mark']; ?></td>
			<td><?php echo $examquestion['status']; ?></td>
			<td><?php echo $examquestion['created']; ?></td>
			<td><?php echo $examquestion['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'examquestions', 'action' => 'view', $examquestion['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'examquestions', 'action' => 'edit', $examquestion['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'examquestions', 'action' => 'delete', $examquestion['id']), array('confirm' => __('Are you sure you want to delete # %s?', $examquestion['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Examquestion'), array('controller' => 'examquestions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Results'); ?></h3>
	<?php if (!empty($exam['Result'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Exam Id'); ?></th>
		<th><?php echo __('Attented'); ?></th>
		<th><?php echo __('Wright'); ?></th>
		<th><?php echo __('Wrong'); ?></th>
		<th><?php echo __('Totalmark'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($exam['Result'] as $result): ?>
		<tr>
			<td><?php echo $result['id']; ?></td>
			<td><?php echo $result['exam_id']; ?></td>
			<td><?php echo $result['attented']; ?></td>
			<td><?php echo $result['wright']; ?></td>
			<td><?php echo $result['wrong']; ?></td>
			<td><?php echo $result['totalmark']; ?></td>
			<td><?php echo $result['created']; ?></td>
			<td><?php echo $result['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'results', 'action' => 'view', $result['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'results', 'action' => 'edit', $result['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'results', 'action' => 'delete', $result['id']), array('confirm' => __('Are you sure you want to delete # %s?', $result['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Result'), array('controller' => 'results', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
