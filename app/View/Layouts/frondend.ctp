<?php 
 $user = $this->Session->read('Auth.User'); 
 $exam_session = $this->Session->read('exam_session');
 ?>
<!doctype html>
 <html class="no-js" lang="fr"> 
    <head>
        <meta charset="utf-8">
        <title>My Gurujee</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href='https://fonts.googleapis.com/css?family=Italianno' rel='stylesheet'>
        <link href="https://fonts.googleapis.com/css?family=El+Messiri" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->webroot; ?>assets/img/favicon.png">
        
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
        <link rel="stylesheet" href="<?php echo $this->webroot;?>assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $this->webroot;?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $this->webroot;?>assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo $this->webroot;?>assets/css/responsive.css">
        <link rel="stylesheet" href="<?php echo $this->webroot;?>assets/style.css">

      <script src="<?php echo $this->webroot; ?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
      <script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
      <script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
      <script src="<?php echo $this->webroot; ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
      
      <link href="<?php echo $this->webroot; ?>assets/news-slider.css" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
      <script src="<?php echo $this->webroot; ?>assets/news-slider.js"></script>
      <!-- <link href="<?php echo $this->webroot; ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/> -->

        
        <!-- <link rel="icon" href="img/favicon.ico" type="image/x-icon"> -->
        <!--[if lt IE 9]>
            <script src="js/vendor/html5-3.6-respond-1.4.2.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <div class="loader"></div>
    <a href="#home" class="scrolltotop" style="display: inline;"><i class="fas fa-arrow-up"></i></a>
    <div id="banner" class="banner">
        <div class="backwrap">
                <canvas id="world"></canvas>
                        <div class="lng">
                            <div class="container">
                                 <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="language">
                                           <?php if(!empty($user) && $user['group_id']==2 && $user['status']==1){?>
                                          <li class="active"><a href="#" style="font-weight: 400!important;text-transform: uppercase;font-size: 18px;"><?php echo $user['name']; ?></a></li>
                                            <li>&nbsp;</li>
                                            <li><a href="#">&nbsp;&nbsp;</a></li>
                                            
                                        <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                      <div class="header">
                            <div class="container">
                                    <div class="row">
                                          <div class="col-md-3">
                                              <a href="#">
                                                <div class="imgLogo"></div>
                                              </a>
                                          </div>
                                         
                                        <?php echo  $this->element('menubar');?>
                                         
                                </div>
                            </div>
                      </div>
                      <!-- <div class="bannerDown">
                          <div class="container">
                              <div class="row">
                                  <div class="col-md-6 col-xs-12">
                                      <div class="courseLg">
                                          <h2>LIFE</h2><span>SCIENCES</span>  
                                          <p class="clear">The branches of science that study living things are referred to as the life sciences.Who works in the life sciences would be interested in learning more about plants, animals, human beings or even tiny microscopic organisms. It's defined as the study of life and how living things survive and change. </p>
                                          <div class="btns">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="bannerStudent1"></div>
                                  </div>
                              </div>
                          </div>
                      </div> -->
          </div>
    </div>         
    <?php echo $this->fetch('content');?>
    
    <footer>
        <div class="container">
            <div class="row">
                    <div class="col-md-3 col-md-12">
                        <div class="footerAbout">
                            <div class="logoFooter"></div>
                            <p class="p1">This website aims at empowering the average student. Learn and access only what is required by them - <br>
                            a self-study portal.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <!-- <div class="col-md-6 col-xs-12"> -->
                            <div class="homepages">
                                <h3>PAGES</h3>
                                <ul>
                                    <li class="arrow"><a href="<?php echo $this->webroot; ?>">Home</a></li>
                                    <li class="arrow"><a href="<?php echo $this->webroot; ?>pages/aboutus">About</a></li>
                                    <li class="arrow"><a href="<?php echo $this->webroot; ?>pages/contactus">Contact</a></li>
                                    <li class="arrow"><a href="<?php echo $this->webroot; ?>pages/questions">Question</a></li>
                                    <li class="arrow"><a href="<?php echo $this->webroot; ?>pages/howtoapply">How to apply</a></li>
                                    <!-- <li class="arrow"><a href="<?php echo $this->webroot; ?>pages/registration">Login</a></li> -->
                                </ul>
                            </div>
                        <!-- </div> -->
                    </div>
                    <div class="col-md-3 col-xs-12">
                        <div class="infoFooter">
                            <h3>INFO CONTACT</h3>
                            <div class="info1">
                                <ul>
                                    <li><strong>Email :</strong> info@mygurujee.in</li>
                                    <li><strong>Website :</strong> www.mygurujee.in</li>
                                    <li><strong>Telephone :</strong> 8075567847 </li>
                                    <li><strong>Address :</strong>Institution Name,<br>
                                       Floor, Building, Street, Place/ District 
                                    </li>
                                </ul>
                            </div>
                            <!-- <div class="social2">
                              <ul>
                                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                  <li><a href="#"><i class="fab fa-google"></i></a></li>
                                  <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                              </ul>
                          </div> -->
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12">
                       <div class="infoFooter">
                        <h3>Follow Us</h3>
                          <div class="social2">
                              <ul>
                                  <li><a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a></li>
                                  <li><a href="https://twitter.com/"><i class="fab fa-twitter"></i></a></li>
                                  <li><a href="https://accounts.google.com/"><i class="fab fa-google"></i></a></li>
                                  <li><a href="https://in.linkedin.com/"><i class="fab fa-linkedin-in"></i></a></li>
                              </ul>
                          </div>
                      </div>
                    </div>

            </div>
        </div>
    </footer>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>© <?php echo date("Y");?> <i class="fas fa-heart"></i>My Gurujee.   All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
        <!-- <script src="<?php echo $this->webroot;?>assets/js/vendor/jquery-2.2.4.js"></script> -->
        <script src="<?php echo $this->webroot;?>assets/js/vendor/bootstrap.min.js"></script>
        <!-- <script src="<?php echo $this->webroot;?>assets/js/jquery.js"></script> -->
        <!-- <script src="<?php echo $this->webroot;?>assets/js/html5lightbox.js"></script> -->
        <!-- <script src="<?php echo $this->webroot;?>assets/js/contact-form.js"></script> -->
        <script src="<?php echo $this->webroot;?>assets/js/main.js"></script>

        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5ca388161de11b6e3b067dc4/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->

    </body>
</html>
<style type="text/css">
 .header {
    background-color:#888e9d!important;
  }
  #world{
   height:0px!important; 
  }
  .banner{
    height:150px!important;
  }
  .backwrap{
    width: 100%;
    height: 900px;
    left: 0;
    top: 0;
    z-index: 1;
    background: #888e9d no-repeat center center;
  }
 .breadcrumbs1 {
    /*background: #2b2c2c;*/
    /*background: #FFF;*/
    /*padding: 100px 0 86px 0;*/
}
.breadcrumbs1.container{
background: #FFF;
}
ul.breadcrumb {
  
  list-style: none;
  background-color: #FFF;

  padding: 10px 16px ;
  margin: 5px;
}
ul.breadcrumb li {
  display: inline;
  font-size: 18px;
  color: #fff;
}
ul.breadcrumb li+li:before {
  padding: 8px;
  color: #fff;
  content: "/\00a0";
}
ul.breadcrumb li a {
  color: #e42323;
  text-decoration: none;
  font-weight: 400;
}
ul.breadcrumb li a:hover {
  color: #fff;
  /*text-decoration: underline;*/
}
 .about .poster {
    background:transparent;
    /* -webkit-box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.35); */
    /*-moz-box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.35);*/
    /* box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.35); */
    padding: 30px;
    overflow: hidden;
}
 .h3{
  font-family: 'El Messiri', sans-serif;
  font-size: 30px;
  line-height: 25px;
  font-weight: 400;
 }
@media (max-width: 991px)
.about .poster {
    margin-top: 30px;
}
.h2{
  text-transform: none!important;
  font-family: 'Italianno';
}

.has-error .help-block{
color: #591009 !important;  
}
.alert-danger{
color: #591009 !important;  
}
footer .footerAbout .p1 {
    font-weight: 400;
}
p{
  font-weight: 400;
}
</style>