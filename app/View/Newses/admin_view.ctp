<!-- <div class="newses view">
<h2><?php echo __('Newse'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($newse['Newse']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Heading'); ?></dt>
		<dd>
			<?php echo h($newse['Newse']['heading']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dates'); ?></dt>
		<dd>
			<?php echo h($newse['Newse']['dates']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($newse['Newse']['image']); ?>
			&nbsp;
		</dd>
		
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($newse['Newse']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($newse['Newse']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($newse['Newse']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Newse'), array('action' => 'edit', $newse['Newse']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Newse'), array('action' => 'delete', $newse['Newse']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $newse['Newse']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Newses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Newse'), array('action' => 'add')); ?> </li>
	</ul>
</div>
 -->
<?php $this->Html->addCrumb('News', '/admin/newses'); ?>
<?php $this->Html->addCrumb('View', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('View News'); ?>                        				</div>
	                        <div class="tools">
	                            <a href="javascript:;" class="collapse"></a>
	                        </div>
	                    </div>
                    	<div class="portlet-body form universities">
		                    <div class="alert alert-danger display-hide">
		                        <button data-close="alert" class="close"></button>
		                        You have some form errors. Please check below.
		                    </div>
		                    <!-- BEGIN FORM-->
		                    <?php echo $this->Form->create('News', array('class' => 'form-horizontal')); ?>
		                    <div class="form-body">                      
		                        						<div class='form-group'>
							 <?php echo $this->Form->label('Heading:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($newse['Newse']['heading']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('date:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($newse['Newse']['dates']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('details:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($newse['Newse']['details']);?></p>
							</div>
						</div>
						<div class='form-group'>
							 <?php echo $this->Form->label('image:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><img src="<?php echo $this->webroot.$ImageNews.$newse['Newse']['image']; ?>" style ="float: left;"></p>
							</div>
						</div>
						
						<div class='form-group'>
							 <?php echo $this->Form->label('status:' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<p class="form-control-static"><?php echo h($newse['Newse']['status']);?></p>
							</div>
						</div>
		                            <div class="form-actions fluid">
		                                <div class="col-md-offset-3 col-md-9">
		                                    <button type="button" class="btn blue" onclick="window.location='<?php echo $this->webroot.'admin/newses'; ?>'">Go Back</button>
		                                </div>
		                            </div>
		                        </div>
		                        <?php echo $this->Form->end(); ?>
		                        <!-- END FORM-->
		                    </div>
                	</div>
            </div>
        </div>
    </div>
</div>