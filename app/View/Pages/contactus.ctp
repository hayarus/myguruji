<script type="text/javascript">
$(document).ready(function(){
var error1 = $('.alert-danger');
$('#contactus').validate({
errorElement: 'span', //default input error message container
errorClass: 'help-block', // default input error message class
focusInvalid: false, // do not focus the last invalid input
ignore: "",
rules:{
"data[name]" : {required : true },
"data[emailid]" : {required: true,email: true},
"data[message]" : {required : true,minlength:10 },
},
messages:{
"data[name]": {required :"Please enter name."},
"data[emailid]" : {required : "Please enter a valid email address",
             minlength : "Please enter a valid email address"},
"data[message]" : {required :"Please enter message."},

// "data[Image][name]" : {required :"Please select image.",accept: "Choose a valid file format."},
// "data[Image][status]" : {required :"Please enter status."}
},

invalidHandler: function (event, validator) { //display error alert on form submit              
//success1.hide();
error1.show();
//App.scrollTo(error1, -200);
},

highlight: function (element) { // hightlight error inputs
$(element)
.closest('.form-group').addClass('has-error'); // set error class to the control group
},

unhighlight: function (element) { // revert the change done by hightlight
$(element)
.closest('.form-group').removeClass('has-error'); // set error class to the control group
},

success: function (label) {
label
.closest('.form-group').removeClass('has-error'); // set success class to the control group
label
.closest('.form-group').removeClass('error');
},
});
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDWml9un8St2k_w9Ha2SJ2B1G90S9sXxo"></script>
<script src="<?php echo $this->webroot;?>assets/js/maps.active.js"></script>
<section id="services" class="services">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>CONTACT </h2>
                   <!--  <div class="col-md-6 col-md-offset-3">
                        <p class="p1">Donec sit amet tristique libero. Phasellus tempus accumsan nisl, sit amet gravida nulla tristique finibus. Proin aliquet varius bibendum.</p>
                    </div> -->
                    <!--<div class="col-md-3 col-sm-6 col-xs-12 padding-bottom">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fab fa-font-awesome-flag"></i>
                            </div>
                            <h2> My Choice… My Gurujee</h2>
                            <p class="p3">Aliquam sodales est porta est blandit scelerisque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque eu maximus tortor, a volutpat orci. Pellentesque in odio finibus.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 padding-bottom">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fas fa-cogs"></i>
                            </div>
                            <h2>Be Assured</h2>
                            <p class="p3">Aliquam sodales est porta est blandit scelerisque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque eu maximus tortor, a volutpat orci. Pellentesque in odio finibus.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 padding-bottom">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fas fa-comments"></i>
                            </div>
                            <h2>With you…. All the way….</h2>
                            <p class="p3">Aliquam sodales est porta est blandit scelerisque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque eu maximus tortor, a volutpat orci. Pellentesque in odio finibus.</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 padding-bottom1">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fas fa-trophy"></i>
                            </div>
                            <h2>The success mantra</h2>
                            <p class="p3">Aliquam sodales est porta est blandit scelerisque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque eu maximus tortor, a volutpat orci. Pellentesque in odio finibus.</p>
                        </div>
                    </div>
                     <div class="col-md-4 col-sm-6 col-xs-12 padding-bottom1"> 
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fas fa-list"></i>
                            </div>
                            <h2>CHOOSE A LANGUAGE</h2>
                            <p class="p3">Aliquam sodales est porta est blandit scelerisque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque eu maximus tortor, a volutpat orci. Pellentesque in odio finibus.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fas fa-comment"></i>
                            </div>
                            <h2>SUPPORT 24/7</h2>
                            <p class="p3">Aliquam sodales est porta est blandit scelerisque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque eu maximus tortor, a volutpat orci. Pellentesque in odio finibus.</p>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <section id="contact" class="contact">
        <div class="google-map">
            <div id="map" style="height:500px; width:100%;"></div>
        </div>
        <div class="container">
            <div class="row">
                  <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-1 right1 right-margin">
                      <div class="boxContact">
                          <h3>INFO CONTACT</h3>
                          <div class="info">
                              <ul>
                                 <li><strong>Address :</strong> Institution Name,<br>
                                       Floor, Building, Street, Place/ District 
                                <li><strong>Telephone:</strong> 8075567847</li>
                                <li><strong>Email:</strong> info@mygurujee.in</li>
                                <li><strong>Website:</strong> www.mygurujee.in</li>
                              </ul>
                          </div>
                          <!-- <a href="#"><div class="logo2"></div></a> -->
                          
                          <div class="social1">
                              <ul>
                                  <li><a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a></li>
                                  <li><a href="https://twitter.com/"><i class="fab fa-twitter"></i></a></li>
                                  <li><a href="https://accounts.google.com/"><i class="fab fa-google"></i></a></li>
                                  <li><a href="https://in.linkedin.com/"><i class="fab fa-linkedin-in"></i></a></li>
                              </ul>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </section>

        <section id="about" class="about">
        <div class="container">
            <div class="row">
                <!--  -->
                <div class="col-md-6 col-xs-12">
                   <h2>MESSAGES</h2>
                    <div class="box">
                      <p class="form-messege"></p>
                        <form  name="contactus" id="contactus" class="form-horizontal" role="form" method="post">
                              <div class="form-group" id="">
                                    <!-- <input type="text" class="form-control" id="form-name" name="form-name" placeholder="Your Name...*" required> -->
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false,'placeholder'=>'Your Name...', 'required' => false));?>
                              </div>
                              <div class="form-group" id="">
                                    <!-- <input type="email" class="form-control" id="form-email" name="form-email" placeholder="Your Email...*" required> -->
                                    <?php echo $this->Form->input('emailid', array('class' => 'form-control','type'=>'email','label' => false,'placeholder'=>'Your Email...', 'required' => false));?>
                              </div>
                              <div class="form-group" id="">

                                    <!-- <input type="text" class="form-control" id="form-phone" name="form-phone" placeholder="Number Mobile..." required> -->
                                    <?php echo $this->Form->input('message', array('class' => 'form-control','type'=>'textarea','label' => false,'placeholder'=>'Your Message...', 'required' => false));?>
                              </div>
                             
                              <div class="right">
                                    <button type="submit" class="purple1">SENT</button>
                              </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 message">
                    <h2></h2>
                    <p></p>
                    <br/>
                    <p>Inform Us Your Queries And Suggestions </p>
                    <br/>
                    <img  src="<?php echo $this->webroot;?>assets/img/logofinal.jpg" width="50%" height="50%">
                    <!-- <div class="btn1">
                        <a href="#" class="orange">Learn More</a>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <style type="text/css">
      .message img {
       
        display: block;
        margin-left: auto;
        margin-right: auto;
      }
      .message p{
        text-align: center!important;
      }
    </style>
