 <?php 
 $user = $this->Session->read('Auth.User'); 

 ?>
 <section id="about" class="about">
        <div class="container">
            <div class="row">
               
                <div class="col-md-6 col-xs-12">
                    <h2>HELLO &nbsp;<?php echo $user['name'];?></h2>
                    <p>The exams are conducted as MCQ with a maximum of 200 marks. The question paper contains three parts:Part 'A' would be common to all subjectswith a maximum of 20 questions of General Aptitude. Part 'B'contains subject-related conventional MCQs with maximum number of questions in the range of 20-35. Part 'C' containsanalytical questions based on scientific concepts and/or application of the scientific concepts.</p>
                    <br/>
                </div>
               <div class="col-md-6 col-xs-12">
                    <div>
                        <img src="<?php echo $this->webroot;?>assets/img/onlineexam.png" width="400px" height="320px">
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
         <div class="container">
            <div class="row">
               <div class="box">  
               <div class="col-md-12 col-xs-12">
                   
                    <h3>CHOOSE A EXAM PART</h3>.
                    <!-- <p>The exams are conducted as MCQ with a maximum of 200 marks. The question paper contains three parts:Part 'A' would be common to all subjectswith a maximum of 20 questions of General Aptitude. Part 'B'contains subject-related conventional MCQs with maximum number of questions in the range of 20-35. Part 'C' containsanalytical questions based on scientific concepts and/or application of the scientific concepts.</p>
                    <br/> -->
                    <ul>
                        <?php foreach ($examparts as $key =>$exampart) {
                        ?>

                        <!-- <li><a href="<?php echo $key;?>"><i></i><?php echo "Exam ".$exampart;?></a></li> -->
                       <li> <?php echo $this->Form->postlink(__("Exam ".$exampart), array('controller'=>'pages','action' => 'exampart','?'=>array( 'id'=> $user['id'] , 'part'=>$key))); ?></li>
                        <!-- <li><?php echo $key;?></li> -->


                        <?php } ?>
                    </ul>
                </div>
               <!-- <div class="col-md-6 col-xs-12">
                    <div>
                        <img src="<?php echo $this->webroot;?>assets/img/onlineexam.png" width="400px" height="320px">
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>
<style>
.about h3 {
    font-size: 20px;
    text-align: left;
    font-weight: 400;
    color: #e42323;
    margin-bottom: 30px;
    text-transform: uppercase;
}
.box ul {
    list-style: none;
    width: 1050px;
    margin: 0 auto;
    display: table;
}
.box ul a {
    background: rgb(111,122,153);
    background: -moz-linear-gradient(top, rgba(111,122,153,1) 0%, rgba(54,58,70,1) 100%);
    background: -webkit-linear-gradient(top, rgba(111,122,153,1) 0%,rgba(54,58,70,1) 100%);
    background: linear-gradient(to bottom, rgba(111,122,153,1) 0%,rgba(54,58,70,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#6f7a99', endColorstr='#363a46',GradientType=0 );
    border-radius: 30px;
    text-transform: uppercase;
    color: #FFF;
    text-align: center;
    padding: 10px 0;
    margin-right: 35px;
    float: left;
    cursor: pointer;
    margin-bottom: 25px;
}
.box ul {
    list-style: none;
    width: 1050px;
    margin: 0 auto;
    display: table;
}
.box ul a {
    text-transform: uppercase;
    color: #FFF;
    text-decoration: none;
    width: 100%;
    height:100%;

    }
.box ul a:hover {
    background: -moz-linear-gradient(top, #17d0cf 0%, #01a6fd 51%, #17d0cf 100%);
    background: -webkit-linear-gradient(left, #17d0cf 0%, #01a6fd 51%, #0071b0 100%);
    background: linear-gradient(to bottom, #17d0cf 0%, #01a6fd 51%, #17d0cf 100%);
    background-size: 200% auto;
    background-position: 190px;
}

/*responsive*/
@media (min-width: 768px) {
    .box ul a {
        width: 300px;
        font-size: 18px;
        height: 60px;
        line-height: 40px;
    } 
}
@media (max-width: 991px)
{
.box ul a {
    margin-bottom: 30px;
    margin-right: 10px !important;
}
}
@media (max-width: 991px){
.box ul {
    padding: 0;
}
}

@media (max-width: 1200px){
.box ul {
    width: 300px !important;
}
}
@media (max-width: 991px){
.box ul a {
    margin-bottom: 30px;
    margin-right: 10px !important;
}
}
@media (max-width: 768px){
.box ul a {
    width: 275px !important;
    font-size: 15px;
    height: 45px;
    line-height: 25px;
    margin-right: 0 !important;
}
}
</style>