<?php 
 $user = $this->Session->read('Auth.User'); 
 $exam_session = $this->Session->read('exam_session');
 ?>
<section id="about" class="about">
        <div class="container">
            <div class="row">
               
                <div class="col-md-8 col-xs-12">
                    <h2>LIFE SCIENCE EXAM RESULT PART <?php echo  $exam_session['exampart']; ?></h2>
                    <p>The Red Color Indicates Wrong Answer And Green Color Indicates Right Answer. If Your Answer is Wrong You Can See Right Answer Of The Question Listed Near Your Answer In Green Color</p>
                    <br/>
                </div>
               <!-- <div class="col-md-6 col-xs-12">
                    <div>
                        <img src="<?php echo $this->webroot;?>assets/img/onlineexam.png" width="400px" height="320px">
                    </div>
                </div> -->
    </div>
</div>
<br>
<br>
 <div class="container">
    <div class="row">
       <div class="box">  
       <div class="col-md-12 col-xs-12">
        <p> Name Of Candidate  : <?php echo $user['name']; ?></p>
        <p> Mark Collected     : <?php echo $mark; ?></p>
        
        <br>
        <h2>Exam Sheet</h2>


        <?php foreach ($examsheets as $examsheet) { ?>
            <h3><?php echo $examsheet['Question']['question'].'&nbsp?';  ?></h3>
            <ul>
                <?php if($examsheet['Examquestion']['mark'] == 2){ ?>
            <li class="li2"><?php echo $examsheet['Examquestion']['answer'];?></li>
            
        <?php } else { ?>
            <li class="li1"><?php echo $examsheet['Examquestion']['answer'];?></li>
            <li class="li2"><?php echo $examsheet['Question']['question'];?></li>

        <?php } ?>
            </ul>  

            <?php
        } ?>
       <!-- <div class="col-md-6 col-xs-12">
            <div>
                <img src="<?php echo $this->webroot;?>assets/img/onlineexam.png" width="400px" height="320px">
            </div>
        </div> -->
    </div>
</div>
</div>
</section>

<style>
.about h3 {
    font-size: 20px;
    text-align: left;
    font-weight: 400;
    color: #e42323;
    margin-bottom: 30px;
    text-transform: capitalize;
}
.li1 {
    background: rgba(153, 111, 111, 1);
    background: -moz-linear-gradient(top, rgba(111,122,153,1) 0%, rgba(54,58,70,1) 100%);
    background: -webkit-linear-gradient(top, rgb(153, 111, 111) 0%,rgb(70, 55, 54) 100%);
    background: linear-gradient(to bottom, rgb(255, 10, 10) 0%,rgb(129, 0, 0) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#6f7a99', endColorstr='#363a46',GradientType=0 );
    border-radius: 30px;
    text-transform: capitalize;
    color: #FFF;
    text-align: center;
    padding: 10px 0;
    margin-right: 35px;
    float: left;
    /*cursor: pointer;*/
    margin-bottom: 25px;
}
.box ul {
    list-style: none;
    width: 1050px;
    margin: 0 auto;
    display: table;
}
.box ul li {
    text-transform: capitalize;
    color: #FFF;
    text-decoration: none;
    width: 100%;
    height:100%;

    }
.li2 {
    background: -moz-linear-gradient(top, #17d0cf 0%, #01a6fd 51%, #17d0cf 100%);
    background: -webkit-linear-gradient(left, #17d056 0%, #01fdda 51%, #00b03e 100%);
    background: linear-gradient(to bottom, #17d069 0%, #01fd67 51%, #17d069 100%);
    background-size: 200% auto;
    background-position: 190px;
    border-radius: 30px;
    text-transform: capitalize;
    color: #FFF;
    text-align: center;
    padding: 10px 0;
    margin-right: 35px;
    float: left;
    /*cursor: pointer;*/
    margin-bottom: 25px;
}
/*responsive*/
@media (min-width: 768px) {
    .box ul li {
    width: 275px;
    font-size: 18px;
    height: 45px;
     line-height: 25px; 
    } 
}
@media (max-width: 991px)
{
.box ul li {
    margin-bottom: 30px;
    margin-right: 10px !important;
}
}
@media (max-width: 991px){
.box ul {
    padding: 0;
}
}

@media (max-width: 1200px){
.box ul {
    width: 400px !important;
}
}
@media (max-width: 991px){
.box ul li {
    margin-bottom: 30px;
    margin-right: 10px !important;
}
}
@media (max-width: 768px){
.box ul li {
    width: 275px !important;
    font-size: 15px;
    height: 45px;
    line-height: 25px;
    margin-right: 0 !important;
}
}
</style>
</style>