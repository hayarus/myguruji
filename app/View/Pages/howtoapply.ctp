<!--  <div class="breadcrumbs1">
      <div class="container">
            
                 <ul class="breadcrumb">
                      <li><a href="<?php echo $this->webroot;  ?>">Home</a></li>
                      <li>About</li>
                </ul>
              
            </div>
          </div>  --> 
<section id="courses" class="courses">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>HOW TO APPLY</h2>
                    <div class="col-md-6 col-md-offset-3">
                        <p class="p1"> </p>
                    </div>
                    <!-- <ul class="tabs">
                          <li class="active" rel="tab1"><i class="fas fa-pencil-alt"></i> TEST CONDITIONS</li>
                          <li rel="tab2"><i class="fas fa-chart-pie"></i> LEVELING TEST</li>
                          <li rel="tab3"><i class="fas fa-graduation-cap"></i> TEST RESULTS</li>
                    </ul> -->
                    <div class="tab_container">
                          <div id="tab1" class="tab_content">
                                  <p class="p2"> CSIR conducts the Joint CSIR-UGC exam twice in a year (June and December) for the eligibility in awarding Junior Research Fellowships (JRF) and for appointment as Lecturer (LS) in Life Sciences. The exams are conducted as MCQ with a maximum of 200 marks.The question paper contains three parts:Part 'A' would be common to all subjectswith a maximum of 20 questions of General Aptitude. Part 'B'contains subject-related conventional MCQs with maximum number of questions in the range of 20-35. Part 'C' containsanalytical questions based on scientific concepts and/or application of the scientific concepts. The application for CSIR-UGC (NET) exam is online. For more information please click on this link <span> <a href="http://www.csirhrdg.res.in/">www.csirhrdg.res.in</a></span>
                                  </p>
                                  <br/>
                                  <!-- <p class="p2">Mauris lobortis lacus vitae sem malesuada elementum. Phasellus fermentum viverra elit, a venenatis neque fermentum eu. Sed eu est est. Aliquam semper, quam ut vulputate congue, nisi augue commodo nunc, ut rhoncus est nunc sed odio. Nulla aliquam, turpis non interdum cursus, velit dolor scelerisque arcu, sed blandit arcu arcu at massa. Duis a tortor eget nibh tempor ultricies sed at enim. Proin nec scelerisque risus, non interdum quam. Etiam rutrum molestie urna, ut ullamcorper dolor blandit non.</p> -->
                                  <!-- <div class="btn2">
                                      <a href="#" class="orange">Read More</a>
                                  </div> -->
                          </div>
                          <!-- #tab1 -->
                          <!-- <div id="tab2" class="tab_content">
                                  <p class="p2">Donec sit amet tristique libero. Phasellus tempus accumsan nisl, sit amet gravida nulla tristique finibus. Proin aliquet varius bibendum. Curabitur eleifend nisi enim, sed hendrerit tellus volutpat eget. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, congue nec vestibulum ut, commodo lobortis metus.</p>
                                  <br/>
                                  <p class="p2">Mauris lobortis lacus vitae sem malesuada elementum. Phasellus fermentum viverra elit, a venenatis neque fermentum eu. Sed eu est est. Aliquam semper, quam ut vulputate congue, nisi augue commodo nunc, ut rhoncus est nunc sed odio. Nulla aliquam, turpis non interdum cursus, velit dolor scelerisque arcu, sed blandit arcu arcu at massa. Duis a tortor eget nibh tempor ultricies sed at enim. Proin nec scelerisque risus, non interdum quam. Etiam rutrum molestie urna, ut ullamcorper dolor blandit non.</p>
                                  <div class="btn2">
                                      <a href="#" class="orange">Read More</a>
                                  </div>
                          </div> -->
                          <!-- #tab2 -->
                          <!-- <div id="tab3" class="tab_content">
                                  <p class="p2">Donec sit amet tristique libero. Phasellus tempus accumsan nisl, sit amet gravida nulla tristique finibus. Proin aliquet varius bibendum. Curabitur eleifend nisi enim, sed hendrerit tellus volutpat eget. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, congue nec vestibulum ut, commodo lobortis metus.</p>
                                  <br/>
                                  <p class="p2">Mauris lobortis lacus vitae sem malesuada elementum. Phasellus fermentum viverra elit, a venenatis neque fermentum eu. Sed eu est est. Aliquam semper, quam ut vulputate congue, nisi augue commodo nunc, ut rhoncus est nunc sed odio. Nulla aliquam, turpis non interdum cursus, velit dolor scelerisque arcu, sed blandit arcu arcu at massa. Duis a tortor eget nibh tempor ultricies sed at enim. Proin nec scelerisque risus, non interdum quam. Etiam rutrum molestie urna, ut ullamcorper dolor blandit non.</p>
                                  <div class="btn2">
                                      <a href="#" class="orange">Read More</a>
                                  </div>
                          </div> -->
                          <!-- #tab3 -->
                        </div>
                        <!-- .tab_container -->
                    </div>
                </div>
            </div>
    </section>
    <section id="about" class="about">
       <!--  <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                  <div class="box">
                    <h2>ABOUT CSIR-UGC FELLOWSHIP</h2>
                    <p>On-line applications for JRF/NET are invited twice a year on all India basis through press Notification. The Notification is also made available on HRDG website (www.csirhrdg.res.in).</p><br>

                    <p>CSIR and UGC provide Research Fellowships to bright young men and women for training inmethods of research under expert guidance of faculty members/scientists working in University Department/ National Laboratories and Institutions in various fields of Science. Only bonafide Indian citizens are eligible for this test</p>
                    
                    <p> CSIR/UGC Fellowships are tenable in Universities/IITs/Post Graduate Colleges/Govt. Research
                    Establishments including those of the CSIR, Research & Development establishments of recognized
                    public or private sector industrial firms and other recognized institutions.</p>

                    <p>CSIR/UGC Fellowship is tenable in India. The programme is aimed at National Science & Technology Human Resource Development.</p>
                    
                    <p>A large number of JRFs are awarded each year by CSIR to candidates holding BS-4 years program/ BE/B.Tech/B. Pharma/MBBS/ Integrated BS-MS/M.Sc. or Equivalent degree/B.Sc (Hons) or  equivalent degree holders or students enrolled in integrated MS-Ph.D program with at least  55% marks for General & OBC (50% for SC/ST candidates and Persons with Disability) after qualifying the Test conducted by CSIR twice a year in June and December.</p>
                    
                    <p>This Test also determines the eligibility of candidates (i.e it is eligibility criteria only) for Lectureship positions in Indian University/Colleges. Those who qualify for JRF are eligible for Lectureship also, subject to fulfilling the eligibility criteria laid down by UGC. Some aspirants are declared successful in eligibility for Lectureship only based on their performance in the test.</p>
                    <p>The award of CSIR/UGC Fellowship is for a fixed tenure and does not imply any assurance or guarantee for subsequent employment by CSIR/UGC to the beneficiary.</p>
                    <br>
                  
                    
                </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="box">
                    <h2>UPPER AGE LIMIT</h2>
                    <p> For Junior Research Fellowship (JRF): Candidate should not be more than 30 years for December / June Exam of the corresponding year, relaxable upto 5 years in case of candidates belonging to SC/ST/OBC/PH/VH and women applicants. Relaxation will also be provided to the candidates having research experience, limited to the period spent on research in the relevant/related subject of post-graduation degree, subject to a maximum of 5 years, on production of a certificate from appropriate authority. Three years relaxation in age will be permissible to the candidates possessing L.L.M. Degree.  For Lecturership: There is no upper age limit.</p>
                    
                </div>
               </div>
            </div>
        </div>
         <br> -->
<div class="container">
            <div class="row">
         <div class="col-md-12 col-xs-12">
          <div class="box">
                <div class="content">
                    <h3 class="h3">ABOUT CSIR-UGC FELLOWSHIP</h3>
                    <p>On-line applications for JRF/NET are invited twice a year on all India basis through press Notification. The Notification is also made available on HRDG website (www.csirhrdg.res.in).</p><br>

                    <p>CSIR and UGC provide Research Fellowships to bright young men and women for training inmethods of research under expert guidance of faculty members/scientists working in University Department/ National Laboratories and Institutions in various fields of Science. Only bonafide Indian citizens are eligible for this test</p>
                    
                    <p> CSIR/UGC Fellowships are tenable in Universities/IITs/Post Graduate Colleges/Govt. Research
                    Establishments including those of the CSIR, Research & Development establishments of recognized
                    public or private sector industrial firms and other recognized institutions.</p>

                    <p>CSIR/UGC Fellowship is tenable in India. The programme is aimed at National Science & Technology Human Resource Development.</p>
                    
                    <p>A large number of JRFs are awarded each year by CSIR to candidates holding BS-4 years program/ BE/B.Tech/B. Pharma/MBBS/ Integrated BS-MS/M.Sc. or Equivalent degree/B.Sc (Hons) or  equivalent degree holders or students enrolled in integrated MS-Ph.D program with at least  55% marks for General & OBC (50% for SC/ST candidates and Persons with Disability) after qualifying the Test conducted by CSIR twice a year in June and December.</p>
                    
                    <p>This Test also determines the eligibility of candidates (i.e it is eligibility criteria only) for Lectureship positions in Indian University/Colleges. Those who qualify for JRF are eligible for Lectureship also, subject to fulfilling the eligibility criteria laid down by UGC. Some aspirants are declared successful in eligibility for Lectureship only based on their performance in the test.</p>
                    <p>The award of CSIR/UGC Fellowship is for a fixed tenure and does not imply any assurance or guarantee for subsequent employment by CSIR/UGC to the beneficiary.</p>
                </div>

                <div class="content">
                    <h3 class="h3"> AGE LIMIT</h3>
                    <strong>For JRF (NET): Maximum 28 years as on 01-07-2018</strong><p>upper age limit may be relaxable up to 5 years in case of SC/ST/Persons with Disability(PwD)/female applicants and 03 years in case of OBC (Non Creamy Layer) applicants.</p>
                    <strong>For Lectureship (NET): No upper age limit</strong> 

                </div>

                <div class="content">
                    <h3 class="h3"> EDUCATIONAL QUALIFICATION</h3>
                    <p>M.Sc or equivalent degree/ Integrated BS-MS/BS-4 years/BE/BTech/BPharma/MBBS with at least 55% marks for General (UR) and OBC candidates and 50% for SC/ST, Persons with Disability (PwD) candidates.</p>

                    <p>Candidates enrolled for M.Sc or having completed 10+2+3 years of the above qualifyingexamination as on the closing date of online submission of application form, are also eligible to apply in the above subject under the Result Awaited (RA) category on the condition that they complete the qualifying degree with requisite percentage of marks within the validity period of two years to avail the fellowship from the effective date of award.</p>

                    <p>Such candidates will have to submit the attestation form duly certified by the Head of the Department/Institute from where the candidate is appearing or has appeared.</p>

                    <p>B.Sc (Hons) or equivalent degree holders or students enrolled in Integrated MS-PhD program with at least 55% marks for General (UR) and OBC candidates; 50% marks for SC/ST, Persons with Disability (PwD) candidates are also eligible to apply. Candidates with bachelor’s degree will be eligible for CSIR fellowship only after getting registered/enrolled for Ph.D/Integrated Ph.D program within the validity period of two years.</p>
                   <p>Candidates possessing only Bachelor’s degree are eligible to apply only for Junior Research Fellowship (JRF) and not for Lectureship (LS).</p>
                   <p>Specification of degrees (whether Bachelor’s or Master’s) as notified by the UGC in its Gazette Notification No. F. 5-1/2013 (CPP-II) dated 5th July, 2014 and as amended from time to time, shall apply.</p>
                   <p>The eligibility for lectureship of NET qualified candidates will be subject to fulfilling the criteria laid down by UGC. Ph.D degree holders who have passed Master’s degree prior to 19th September, 1991 with at least 50% marks are eligible to apply for Lectureship only.</p>


                </div>
              </div>

              </div>
              </div>
            </div>
    </section>

<style type="text/css">
  .content{
    padding-top: 30px;
    margin-bottom: 25px;
  }
.tab_content a{
  color :#3498DB;
  font-size: 18px;
  font-weight: 400;

}
.tab_content a:hover{ 
color:#e42323;
 }
</style>