
<section id="about" class="about">
  
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h2>INTRODUCTION</h2>
                    <p>My Gurujee aims at empowering the genius in you and to acheive greatness in life. This institute was started in January 2019 with the focus on providing premium education and training opportunities for CSIR-UGC JRF/NET, GATE, DBT-JRF exams.</p>
                  <!-- <div class="btn1">
                        <a href="<?php echo $this->webroot;?>pages/registration" class="orange"></a>
                    </div> -->
                </div> 
                <div class="col-md-6"> 
                  <h2>Latest News</h2>
                    <div class="row">
                    <div class="col-md-12 news-slider">
                    <?php foreach ($allnews as $news ) {
                           ?> 
                           
                      <div class="slide"> 
                        <div class="blogPost--small"> 
                          <div class="media"> 
                            <span class="pull-left">
                              <a href="#"><span class="date">
                                <span><?php echo date("d", strtotime($news['Newse']['created'])); ?></span> <small><?php echo date("M", strtotime($news['Newse']['created'])); ?></small></span></a></span> 
                                <div class="media-body"> 
                                  <h4 class="media-heading"> 
                                    <a href="<?php echo $this->webroot.'pages/news/'.$news['Newse']['id']; ?>"><?php echo $news['Newse']['heading']; ?></a> 
                                  </h4> 
                                  <p> <?php echo substr($news['Newse']['details'], 0,100); ?>... </p> 
                                </div> 
                              </div> 
                            </div><!-- / blogPost --> 
                            
                          </div><!-- / slider1 -->
                        <?php } ?>
                      </div>
                      <div class="col-md-12 text-right">
                        <button id="ct-js-district-calendar--prev" type="button" class="slick-arrow next"><img src="<?php echo $this->webroot;?>assets/img/arrow-prev.jpg" alt="Previous"></button>
                        <button id="ct-js-district-calendar--next" type="button" class="slick-arrow next"><img src="<?php echo $this->webroot;?>assets/img/arrow-next.jpg" alt="Next"></button>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
  <!--   <section id="intro" class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>INTRODUCTION</h2>
                    <div class="col-md-6 col-md-offset-3">
                        <p class="p1"> </p>
                    </div>
                    <div class="tab_container">
                          <div id="tab1" class="tab_content">
                                  <p class="p2"> This website aims at empowering the average student . To a complete and comprehensive source of study material that allows them to have a fair chance at the CSIR/UGC JRF(NET) exam. To be able to prepare for the exam without the the cut-throat rates charged by other institutions or centers. 
                                   Learn and access only what is required by them - a tailor made study portal.To understand the basic concepts of life science in a nutshell with solved previous question papers.
                                  </p>
                                  <br/>
                          </div>
                        </div>  
                    </div>
                </div>
            </div>
    </section> -->
    <section id="courses" class="courses">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>OUR WAY</h2>
                    <div class="col-md-6 col-md-offset-3">
                        <!-- <p class="p1">Donec sit amet tristique libero. Phasellus tempus accumsan nisl, sit amet gravida nulla tristique finibus. Proin aliquet varius bibendum.</p> -->
                    </div>
                    <ul class="tabs">
                          <li class="active" rel="tab1"><i class="fas fa-pencil-alt"></i> TRAINING</li>
                          <li rel="tab2"><i class="fas fa-chart-pie"></i> TEST PAPER</li>
                          <li rel="tab3"><i class="fas fa-graduation-cap"></i> TEST RESULTS</li>
                    </ul>
                    <div class="tab_container">
                          <div id="tab1" class="tab_content">
                                  <p class="p2">This training is important to be able to  qualify directly for admission to PhD programs in the institute of your choice and for the eligibility in lectureship. Currently PhD has been made mandatory for appointment to a teaching post in colleges and universities and there is only limited number of student intake by approved guides in approved institutions. This course can improve your life decisions by allowing you to access quality education at an affordable price. The benefit from undergoing this training is a clear cut understanding of the topic which provides for fast learning with previous questions of each topic given at the end of each module.</p>
                                  <!-- <div class="btn2">
                                      <a href="#" class="orange">Read More</a>
                                  </div> -->
                          </div>
                          <!-- #tab1 -->
                          <div id="tab2" class="tab_content">
                                  
                                  <p class="p2">You can access model test papers for each module of the CSIR exam syllabus for life sciences. The question papers would be set on each module as a whole and as smaller topics within a module. Each test would be a total of 20-25 questions based on a particular topic that has to be chosen. On submitting the test results, your score and the analysis of your results would be given. The correct answers would be provided with its explanations. The candidate can access his/her test results in the dashboard and improve his/her performance.</p>
                                  <br/>
                                  <!-- <p class="p2">Only paid subscribers can access to each module lessons and practice questions with answers.Subcription fees : Rs.1000 per module for individual modules Maximum of 4 modules, Rs 7000 Lumpsum @ 500 per module for all modules</p> -->
                                  
                                  <!-- <div class="btn2">
                                      <a href="#" class="orange">Read More</a>
                                  </div> -->
                          </div>
                          <!-- #tab2 -->
                          <div id="tab3" class="tab_content">
                                  <p class="p2"></p>
                                   <p class="p2">The test results in the CSIR exam are based on the cut-off marks given by the CSIR. Candidates with higher marks would be awarded JRF while candidates with lower marks qualify for NET/Lectureship. The Cut-off marks are decided by the panel of experts in CSIR. The first set of ranks would be awarded the CSIR JRF fellowship and the candidates who secure lower ranks would be awarded with UGC JRF fellowship. Candidates with even lower ranks would be awarded with NET for lectureship.  </p>
                                  <!-- <div class="btn2"> 
                                      <a href="#" class="orange">Read More</a>
                                  </div> -->
                          </div>
                          <!-- #tab3 -->
                        </div>
                        <!-- .tab_container -->
                    </div>
                </div>
            </div>
    </section>

    <section id="students" class="students">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Exam Qualifiers</h2>
                    <div class="col-md-6 col-md-offset-3">
                        <p class="p1"></p>
                    </div>
                    <div class="clear">
                      <div class="sliders">

                        <div class="col-md-3 padding-bottom1">
                            <div class="boxStudent">
                                <div class="hover">
                                    <div class="social">
                                        <!-- <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fab fa-google"></i></a></li>
                                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                        </ul> -->
                                        <h2 class="h2">Exam Qualifier</h2>
                                        <p class="p">1st Rank UGC Net</p>
                                    </div>
                                </div>
                                <div class="student1"></div>
                            </div>
                        </div>
                        <div class="col-md-3 padding-bottom1">
                            <div class="boxStudent">
                                <div class="hover">
                                    <div class="social">
                                        <!-- <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fab fa-google"></i></a></li>
                                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                        </ul> -->
                                        <h2 class="h2">Exam Qualifier</h2>
                                        <p class="p">2nd Rank UGC Net</p>
                                    </div>
                                </div>
                                <div class="student2"></div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="boxStudent">
                                <div class="hover">
                                    <div class="social">
                                        <!-- <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fab fa-google"></i></a></li>
                                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                        </ul> -->
                                        <h2 class="h2">Exam Qualifier</h2>
                                        <p class="p">10th Rank UGC Net</p>
                                    </div>
                                </div>
                                <div class="student4"></div>
                            </div>
                        </div>
                        <div class="col-md-3 padding-bottom2">
                            <div class="boxStudent">
                                <div class="hover">
                                    <div class="social">
                                        <!-- <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fab fa-google"></i></a></li>
                                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                        </ul> -->
                                        <h2 class="h2">Exam Qualifier</h2>
                                        <p class="p">5th Rank UGC Net</p>
                                    </div>
                                </div>
                                <div class="student3"></div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="boxStudent">
                                <div class="hover">
                                    <div class="social">
                                        <!-- <ul>
                                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fab fa-google"></i></a></li>
                                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                        </ul> -->
                                        <h2 class="h2">Exam Qualifier</h2>
                                        <p class="p">10th Rank UGC Net</p>
                                    </div>
                                </div>
                                <div class="student4"></div>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section id="services" class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>SERVICES</h2>
                    <div class="col-md-12 col-md-offset">
                        <p class="p1">The main aim is to provide quality study material to students that can be easily understood. The modules are set in such a way that students can track out topics very easily.We have made sound training  sessions for competitive CSIR/UGC JRF(NET) exams.Our students have been successful in  CSIR/UGC JRF(NET)  exams.</p>

                    </div>
                    <div class="col-md-3 padding-bottom">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fab fa-font-awesome-flag"></i>
                            </div>
                            <div class="box-title">
                            <h2>Well Prepared Modules</h2>
                            </div>
                            <!-- <div class="box-content">
                            <p class="p3">The main aim is to provide quality study material to students that can be easily understood. The modules are set in such a way that students can track out topics very easily.</p>
                          </div> -->
                        </div>
                    </div>
                    <div class="col-md-3 padding-bottom">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fas fa-cogs"></i>
                            </div>
                            <div class="box-title">
                            <h2>good training sessions</h2>
                            </div>
                            <!-- <div class="box-content">
                            <p class="p3">We have made sound training  sessions for competitive CSIR/UGC JRF(NET) exams.Our students have been successful in  CSIR/UGC JRF(NET)  exams. </p>
                          </div> -->
                        </div>
                    </div>
                    <div class="col-md-3 padding-bottom">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fas fa-comments"></i>
                            </div>
                            <div class="box-title">
                            <h2>communicative channel</h2>
                            </div>
                            <!-- <div class="box-content">
                            <p class="p3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur lorem donec massa sapien.  </p>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-md-3 padding-bottom">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fas fa-comment"></i>
                            </div>
                            <div class="box-title">
                            <h2>SUPPORT 24/7</h2>
                            </div>
                            <!-- <div class="box-content">
                            <p class="p3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur lorem donec massa sapien. </p>
                            </div> -->
                        </div>
                    </div>
                    <!-- <div class="col-md-4 col-sm-6 col-xs-12 padding-bottom1">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fas fa-trophy"></i>
                            </div>
                            <h2>granted carrier</h2>
                            <p class="p3"></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 padding-bottom1">
                        <div class="boxServices">
                            <div class="icon">
                                <i class="fas fa-list"></i>
                            </div>
                            <h2>wings to carrier</h2>
                            <p class="p3"></p>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
<section id="video" class="video">
        <div class="container">
            <div class="row">
              <div class="col-md-6 col-xs-12">
                    <div class="poster">
                        <img src="<?php echo $this->webroot;?>assets/img/caption-1.png">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                  <div class="box-content">
                     <h2 class="h2">My choice... My Gurujee</h2>
						         <p>We dream to help you dream become a JRF holder.</p>
                   </div>
                </div>
            </div>
        </div>
</section>  
   
<!--  -->
 <!--  <section id="video" class="video">
        <div class="container">
            <div class="row">
               
                
                    <div class="col-md-6 col-xs-12 padding-bottom2">
                    <div class="poster">
                      <img src="<?php echo $this->webroot;?>assets/img/bannerStudent1.png" >
                      <div class="hover">
                        <h2 class="h2">The Success Mantra</h2>
                        <p> We aim to bring your full potential to leads your success.The goal is not to become a rank holder but instead of convincing you what is your ability  </p>
                     </div>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12 padding-bottom2">
                    <div class="poster">
                     <img src="<?php echo $this->webroot;?>assets/img/3.png" >
                      <div class="hover">
                        <h2 class="h2">My Choice... My Gurujee</h2>
                        <p>My Gurujee ia a simple and friendly application. It is easier way to access your dream is close to you with one click. In this application create a friendly atmosphere for all of us .Your dream to become a JRF holder, we will help you to catch your dream. </p>
                     </div>
                  </div>
                </div>

               
            </div>
        </div>
    </section> -->


   <style>
  .intro {
    background: #fff;
    padding: 95px 0 100px 0;
  }
.intro h2 {
    font-size: 40px;
    text-align: center;
    font-weight: 500;
    color: #e42323;
    margin-bottom: 30px;
    text-transform: uppercase;
}
.intro .p2 {
    text-align: left;
    clear: both;
}
.box.img{

    background-size: cover;
    width: 100%;
    height: 100%;

}
.img{
    height: 50%;
    width:50%;
}
.marquee {
    font-style: bold;
    font-size: 20px!important;
}
.news a:hover {
  color: red;
    font-style: bold;
    font-size: 20px!important;
}
.students{
     background: url(<?php echo $this->webroot;?>assets/img/bg/2.jpg);
     padding: 100px 0 84px 0;
}
.students h2{
color: #ffffff;
}
.video.row{
background-color: #9400D3!important;
}
.video{
  
   background: none!important;
}
.poster .hover {
    display: none;
}
.poster:hover .hover {
    background: rgba(242, 239, 239, 0.31);
    position: absolute;
    display: block !important;
    top: 0;
    left: 15px;
    right: 15px;
    bottom: 0;
    cursor: pointer;
}
.poster img {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width:90%; 
  height:461px;
}
  .video .row {

    -webkit-box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.35); 
    -moz-box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.35);
     box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.35); 
   
}
.h2{
  font-size: 50px;
  text-align: left;
  font-weight: 500;
  color: #e42323;
  margin-bottom: 30px;
  text-transform: none!important;
  font-family: 'monotype corsiva';
}
.p{
  
  text-align: center!important;
  color: #fff;
  
}

.news{
  height: 300px;
}

.box-title{
  min-height: 46px;
}

.box-content{
  min-height: 168px;
}
.sliders button{
  opacity: 0;
}
.box-content{
  padding-top: 20%;
  bottom: 0;
}
.box-content p{
  font-family: 'monotype corsiva'; 
  font-size: 25px;
}
</style>
