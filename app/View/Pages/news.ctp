<!--  <div class="breadcrumbs1">
  <div class="container">
        
             <ul class="breadcrumb">
                  <li><a href="<?php echo $this->webroot;  ?>">Home</a></li>
                  <li>About</li>
            </ul>
          
        </div>
      </div>  --> 
<section id="courses" class="courses">
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <h2>LATEST NEWS</h2>
              <!-- <div class="col-md-6 col-md-offset-3">
            </div> -->
          </div>
      </div>
</section>
<section id="about" class="about">
  <div class="container">
      <div class="row">
          <div class="col-md-8 col-xs-12">
          <h3 class="h3"><?php echo h($news['Newse']['heading']); ?></h3> 
          <span class="pull-left"><?php echo date("d-F-Y", strtotime($news['Newse']['created'])); ?> </span>
          <div class="col-md-12"> 
            <div class="image"> 
              <?php if(!empty($news['Newse']['image'])){?> <img src="<?php echo $this->webroot.$ImageNews.$news['Newse']['image']; ?>"/> <?php } ?> 
            </div>
            <div class="news">
            <p><?php echo $news['Newse']['details']; ?></p><br> 
            </div>
          </div>
              
              
              
          </div>
          <div class="col-md-4 col-xs-12">
            <h3>More News</h3> 
                <div class="news"> 
                  <div class="col-md-12 news-slider"> 
                    <?php foreach ($allnews as $news ) {?> 
                      <div class="slide"> <div class="blogPost--small"> 
                        <div class="media"> 
                          <span class="pull-left"> 
                            <a href="#"><span class="date"> <span><?php echo date("d", strtotime($news['Newse']['created'])); ?></span> 
                              <small><?php echo date("M", strtotime($news['Newse']['created'])); ?></small></span></a></span> 
                              <div class="media-body"> 
                                <h4 class="media-heading"> 
                                  <a href="<?php echo $this->webroot.'pages/news/'.$news['Newse']['id']; ?>">
                                    <?php echo $news['Newse']['heading']; ?></a> </h4> 
                                    <p> <?php echo substr($news['Newse']['details'], 0,100); ?>... </p> 
                                  </div> 
                                </div> 
                              </div>
                              <!-- / blogPost --> 
                            </div>
                            <!-- / slider1 -->
                             <?php } ?> 
                           </div> 
                           <div class="col-md-12 text-right"> <button id="ct-js-district-calendar--prev" type="button" class="slick-arrow next"><img src="<?php echo $this->webroot;?>assets/img/arrow-prev.jpg" alt="Previous"></button> 
                            <button id="ct-js-district-calendar--next" type="button" class="slick-arrow next"><img src="<?php echo $this->webroot;?>assets/img/arrow-next.jpg" alt="Next"></button> 
                           </div>
              </div>
          </div>
        </div>
    </div>
  </section>

<style type="text/css">
   .services .boxServices:hover {
    background:#73C6B6!important; /* Old browsers */
    
}
.services .boxServices:hover a {

    text-align: center;
    color: rgb(250,66,37);
    font-weight: 500;
    margin-bottom: 30px;
    text-transform: uppercase;
}
.services{
  background:#ABB2B9!important;
}
.image, .news {
    margin-right: -15px;
    margin-left: -15px;
    margin-bottom: 15px;
}
.image {
  margin-top: 10px;
  margin-bottom: 20px;

}
</style>