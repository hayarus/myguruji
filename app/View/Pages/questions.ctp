<!--  <div class="breadcrumbs1">
      <div class="container">
            
                 <ul class="breadcrumb">
                      <li><a href="<?php echo $this->webroot;  ?>">Home</a></li>
                      <li>Questions</li>
                </ul>
              
            </div>
          </div>  --> 
<!-- <section id="students" class="students">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Previous Question Papers</h2>
                    <div class="col-md-12 col-md-offset">
                        <p class="p1">Fast learning with previous question papers may help you solve questions on your own</p>
                    </div>
                    <div class="clear">
                        <?php foreach ($questionpapers as $questionpaper) {
                            ?>
                          
                        <div class="col-md-3 col-sm-6 col-xs-12 padding-bottom1">
                            <div class="boxStudent">
                                <div class="question">

                                    <p><a href="<?php echo $this->webroot.$ImageQuestions.$questionpaper['Questionpaper']['question']; ?>">
                                        <?php echo $questionpaper['Questionpaper']['subject'];?>
                                            
                                        </a><a href="<?php echo $this->webroot.$PdfAnswers.$questionpaper['Questionpaper']['answerkey']; ?>">Answer Key</a>
                                </div>
                                <div class="student1"></div> 
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                        <div class="col-md-3 col-sm-6 col-xs-12 padding-bottom1">
                            <div class="boxStudent">
                                <div class="hover">
                                    
                                </div>
                                <div class="student2"></div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 padding-bottom2">
                            <div class="boxStudent">
                                <div class="hover">
                                    
                                </div>
                                <div class="student3"></div> 
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="boxStudent">
                                <div class="hover">
                                    
                                </div>
                                <div class="student4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
     <section id="services" class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Previous Question Papers</h2>
                    <div class="col-md-12 col-md-offset">
                        <p class="p1">Fast learning with previous question papers may help you solve questions on your own</p>
                    </div>

                     <?php foreach ($questionpapers as $questionpaper) { ?>
                    <div class="col-md-3 col-sm-6 col-xs-12 padding-bottom">
                        <div class="boxServices">
                            <div class="icon">
                                 <i class="fa fa-file"><a href="<?php echo $this->webroot.$ImageQuestions.$questionpaper['Questionpaper']['question']; ?>"></i>
                            </div>
                            <h2><?php echo $questionpaper['Questionpaper']['subject'];?></h2>
                            <p class="p3"> <a class="link" href="<?php echo $this->webroot.$PdfAnswers.$questionpaper['Questionpaper']['answerkey']; ?>">Click Here for Answer Key</a></p>
                        </div>
                    </div>
                     <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <section id="about" class="about">
        <div class="container">
            <div class="row">
               
                <div class="col-md-6 col-xs-12 all">
                    <h2 class="h2">With you…. All the way….</h2>
                    <p> Nullam vehicula ipsum a arcu cursus vitae congue. Turpis egestas pretium aenean pharetra magna ac placerat vestibulum. Habitant morbi tristique senectus et netus et malesuada fames. Mattis rhoncus urna neque viverra. Leo in vitae turpis massa sed. Nunc sed id semper risus in. Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Quis ipsum suspendisse ultrices gravida dictum fusce ut placerat. Sed vulputate odio ut enim blandit. At consectetur lorem donec massa sapien. Est velit egestas dui id ornare arcu. Cras ornare arcu dui vivamus.</p>
                    <br/>
                </div>
               <div class="col-md-6 col-xs-12">
                    <div class="poster">
                        <img src="<?php echo $this->webroot;?>assets/img/3.png" width="400px" height="320px">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
    .services .boxServices:hover {
    background:#5B2C6F!important; /* Old browsers */
    
}
.services .boxServices:hover a {

    text-align: center;
    color: rgb(250,66,37);
    font-weight: 500;
    margin-bottom: 30px;
    text-transform: uppercase;
}
.boxServices:hover .icon i {
    font-size: 30px;
    color: #5B2C6F!important;
}
.about .poster {
    float:right;
}
.about{
    background-color:#f5f5f5;
}
.about .row{
background-color: #9400D3!important;
-webkit-box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.35); 
-moz-box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.35);
box-shadow: 0px 0px 50px 0px rgba(0,0,0,0.35); 
}
.h2 , .about p {

    color:#fff!important;
}


    </style>