<script type="text/javascript">
$(document).ready(function(){
var error1 = $('.alert-danger');
$('#UserRegistrationForm').validate({
errorElement: 'span', //default input error message container
errorClass: 'help-block', // default input error message class
focusInvalid: false, // do not focus the last invalid input
ignore: "",
rules:{
"data[User][name]" : {required : true },
"data[User][email]" : {required: true,email: true},
"data[User][password]" : {required : true,minlength:10 },
},
messages:{
"data[User][name]": {required :"Please enter name."},
"data[User][email]" : {required : "Please enter a valid email address",
             minlength : "Please enter a valid email address"},
"data[User][password]" : {required :"Please enter password."},
},

invalidHandler: function (event, validator) { //display error alert on form submit              
//success1.hide();
error1.show();
//App.scrollTo(error1, -200);
},

highlight: function (element) { // hightlight error inputs
$(element)
.closest('.form-group').addClass('has-error'); // set error class to the control group
},

unhighlight: function (element) { // revert the change done by hightlight
$(element)
.closest('.form-group').removeClass('has-error'); // set error class to the control group
},

success: function (label) {
label
.closest('.form-group').removeClass('has-error'); // set success class to the control group
label
.closest('.form-group').removeClass('error');
},
});
});
</script>

<section id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h2>Enroll Now</h2>
                    <p>The National Educational Testing Bureau of University Grants Commission (UGC) conducts National Eligibility Test (NET) to determine eligibility for lectureship and for award of Junior Research Fellowship (JRF) for Indian nationals in order to ensure minimum standards for the entrants in the teaching profession and research. We Provide an offer  for who looking for teaching profession on life science</p>
                    <br/>
                    <p>(a)You can subscribe modules Rs.1000 per module for individual modules Maximum of 4 modules.</p>
                    <br/>
                    <p>(b)You can subscribe modules Rs 7000 Lumpsum @ 500 per module for all modules.</p>
                    <br/>
                    <p>The benefit from undergoing this training is a clear cut understanding of the topic which provides for fast learning with previous questions of each topic given at the end of each module.</p>
                </div>
                <br>
                <div class="col-md-6 col-xs-12">
                    <div class="box">
                        <!-- <form action id="contact-form" class="form-horizontal" role="form" method="post"> -->
                          <?php echo $this->Form->create('User', array('class' => 'form-horizontal','method'=>'post')); ?>
                              <div class="form-group" id="">
                                    <!-- <input type="text" class="form-control" id="form-name" name="form-name" placeholder="Your Name...*" required> -->
                                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false,'placeholder'=>'Your Name...', 'required' => false));?>
                              </div>
                              <div class="form-group" id="">
                                    <!-- <input type="email" class="form-control" id="form-email" name="form-email" placeholder="Your Email...*" required> -->
                                    <?php echo $this->Form->input('email', array('class' => 'form-control','type'=>'email','label' => false,'placeholder'=>'Your Email...', 'required' => false));?>
                              </div>
                              <div class="form-group" id="">

                                    <!-- <input type="text" class="form-control" id="form-phone" name="form-phone" placeholder="Number Mobile..." required> -->
                                    <?php echo $this->Form->input('password', array('class' => 'form-control','type'=>'password','label' => false,'placeholder'=>'Your password...', 'required' => false));?>
                              </div>
                              <div class="form-group" id="">
                                    <!-- <input type="text" class="form-control" id="form-courses" name="form-courses" placeholder="What Courses..." required> -->
                                    <!-- <?php echo $this->Form->input('group_id', array('class' => 'form-control','type'=>'hidden','label' => false,'value'=>'2'));?> -->
                              </div>
                              <div class="right">
                                    <button type="submit" class="purple1">Submit Now</button>
                              </div>
                        <!-- </form> -->
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

   