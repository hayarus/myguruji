<?php 
 $user = $this->Session->read('Auth.User'); 
 $exam_session = $this->Session->read('exam_session');
 ?>
<section id="about" class="about">
        <div class="container">
            <div class="row">
               
                <div class="col-md-8 col-xs-12">
                    <h2>LIFE SCIENCE NET EXAM PART <?php echo  $exam_session['exampart']; ?></h2>
                    <!-- <p>The exams are conducted as MCQ with a maximum of 200 marks. The question paper contains three parts:Part 'A' would be common to all subjectswith a maximum of 20 questions of General Aptitude. Part 'B'contains subject-related conventional MCQs with maximum number of questions in the range of 20-35. Part 'C' containsanalytical questions based on scientific concepts and/or application of the scientific concepts.</p> -->
                    <br/>
                </div>
               <!-- <div class="col-md-6 col-xs-12">
                    <div>
                        <img src="<?php echo $this->webroot;?>assets/img/onlineexam.png" width="400px" height="320px">
                    </div>
                </div> -->
    </div>
</div>
<br>
<br>
 <div class="container">
    <div class="row">
       <div class="box">  
       <div class="col-md-12 col-xs-12">
          
           <?php foreach ($questions as $key =>$question) {
                ?>
            <h3><?php echo $question['Question']['question'].' ?'; ?></h3>.
             <?php } ?>
            <!-- <p>The exams are conducted as MCQ with a maximum of 200 marks. The question paper contains three parts:Part 'A' would be common to all subjectswith a maximum of 20 questions of General Aptitude. Part 'B'contains subject-related conventional MCQs with maximum number of questions in the range of 20-35. Part 'C' containsanalytical questions based on scientific concepts and/or application of the scientific concepts.</p>
            <br/> -->
           <?php echo $this->Form->create('Examquestion', array('class' => 'form-horizontal','method'=>'post')); ?>
            <?php echo $this->Form->input('id');?>
          <ul  id="language" onclick="lang1(event);">
                <?php foreach ($questions as $key =>$question) {
                ?>
             <li><?php echo $question['Question']['option_a']; ?></li>
              <li><?php echo $question['Question']['option_b']; ?></li>
              <li><?php echo $question['Question']['option_c']; ?></li>
              <li><?php echo $question['Question']['option_d']; ?></li>
                <?php } ?>
           
           </ul>
           <input type="hidden" class="form-control" name="data[Examquestion][answer]" id="data[answer]">
                <div class="right">
                    <button type="submit" class="purple1">Next</button>
                </div>
            <?php echo $this->Form->end();?>
        </div>
       <!-- <div class="col-md-6 col-xs-12">
            <div>
                <img src="<?php echo $this->webroot;?>assets/img/onlineexam.png" width="400px" height="320px">
            </div>
        </div> -->
    </div>
</div>
</div>
<br>
<div class="container">
    <div class="row">
       <div class="box">  
       <div class="col-md-12 col-xs-12">
        <h3>About the question</h3>
        <?php foreach ($questions as $key =>$question) {
                ?>
        <p><?php echo $question['Question']['explanation']; ?></p>
    <?php } ?>
       </div>
    </div>
   </div>
</div>
</section>
<style>
.about h3 {
    font-size: 20px;
    text-align: left;
    font-weight: 400;
    color: #e42323;
    margin-bottom: 30px;
    text-transform: capitalize;
}
.box ul {
    list-style: none;
    width: 1050px;
    margin: 0 auto;
    display: table;
}
.box ul li {
    background: rgb(111,122,153);
    background: -moz-linear-gradient(top, rgba(111,122,153,1) 0%, rgba(54,58,70,1) 100%);
    background: -webkit-linear-gradient(top, rgba(111,122,153,1) 0%,rgba(54,58,70,1) 100%);
    background: linear-gradient(to bottom, rgba(111,122,153,1) 0%,rgba(54,58,70,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#6f7a99', endColorstr='#363a46',GradientType=0 );
    border-radius: 30px;
    text-transform: capitalize;
    color: #FFF;
    text-align: center;
    padding: 10px 0;
    margin-right: 35px;
    float: left;
    cursor: pointer;
    margin-bottom: 25px;
}
.box ul {
    list-style: none;
    width: 1050px;
    margin: 0 auto;
    display: table;
}
.box ul li {
    text-transform: capitalize;
    color: #FFF;
    text-decoration: none;
    width: 100%;
    height:100%;

    }
.box ul li:hover {
    background: -moz-linear-gradient(top, #17d0cf 0%, #01a6fd 51%, #17d0cf 100%);
    background: -webkit-linear-gradient(left, #17d0cf 0%, #01a6fd 51%, #0071b0 100%);
    background: linear-gradient(to bottom, #17d0cf 0%, #01a6fd 51%, #17d0cf 100%);
    background-size: 200% auto;
    background-position: 190px;
}

/*responsive*/
@media (min-width: 768px) {
    .box ul li {
        width: 400px;
        font-size: 18px;
        height: 60px;
        line-height: 40px;
    } 
}
@media (max-width: 991px)
{
.box ul li {
    margin-bottom: 30px;
    margin-right: 10px !important;
}
}
@media (max-width: 991px){
.box ul {
    padding: 0;
}
}

@media (max-width: 1200px){
.box ul {
    width: 400px !important;
}
}
@media (max-width: 991px){
.box ul li {
    margin-bottom: 30px;
    margin-right: 10px !important;
}
}
@media (max-width: 768px){
.box ul li {
    width: 275px !important;
    font-size: 15px;
    height: 45px;
    line-height: 25px;
    margin-right: 0 !important;
}
}
</style>
<script type="text/javascript">
    function lang1(event) {
    var target = event.target || event.srcElement;
    var answer=event.target.innerHTML;
    // alert(ans);
    document.getElementById("data[answer]").value = answer;
}


</script>