<!-- <div class="questionpapers form">
<?php echo $this->Form->create('Questionpaper'); ?>
	<fieldset>
		<legend><?php echo __('Edit Questionpaper'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Questionpaper.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Questionpaper.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Questionpapers'), array('action' => 'index')); ?></li>
	</ul>
</div> -->

 <?php $this->Html->addCrumb('Question Papers', '/admin/questionpapers'); ?>
<?php $this->Html->addCrumb('Edit', ''); ?>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#QuestionpaperAdminEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Questionpaper][subject]" : {required : true },
		"data[Questionpaper][question]" : { extension: "docx|pdf"},
		"data[Questionpaper][answerkey]" : { extension: "docx|pdf"},
		},
		messages:{
		"data[Questionpaper][subject]" : {required :"Please Enter subject name."},
		"data[Questionpaper][question]" : {extension :"Please select file format pdf or docx."},
		"data[Questionpaper][answerkey]" : {extension :"Please select file format pdf or docx."},

		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
		//success1.hide();
		error1.show();
		//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
		$(element)
		.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
		$(element)
		.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
		label
		.closest('.form-group').removeClass('has-error'); // set success class to the control group
		label
		.closest('.form-group').removeClass('error');
		},
	});
});

</script>


<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box green">
				<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i><?php echo __('Edit Question paper'); ?>                       
				</div>
				<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				</div>
				</div>
					<div class="portlet-body form users">
						<div class="alert alert-danger display-hide">
						<button data-close="alert" class="close"></button>
						You have some form errors. Please check below.
						</div>
						<!-- BEGIN FORM-->
						<?php echo $this->Form->create('Questionpaper', array('class' => 'form-horizontal' ,'enctype'=>'multipart/form-data','method'=>'post')); ?>
						<div class="form-body">  
							<div class="form-group">
								<label class="col-md-3 control-label" for="">&nbsp;</label>
								<div class="col-md-4">
									<div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
								</div>
							</div>                                
							<div class="form-group">
								<!--<?php echo $this->Form->label('id<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?> -->
								<div class='col-md-4'>
									<?php echo $this->Form->input('id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div>
							
							<div class="form-group">
								<?php echo $this->Form->label('subject<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('subject', array('class' => 'form-control', 'label' => false, 'required' => true));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('question<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('question', array('class' => 'form-control', 'label' => false,'type'=>'file', 'required' => false));?>
								</div>
								<!-- <div class="col-md-2"><img src=><?php echo $this->webroot.$ImageQuestions.$this->request->data['Questionpaper']['question']; ?>"</div> -->
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('answer key<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('answerkey', array('class' => 'form-control', 'label' => false,'type'=>'file', 'required' => false));?>
								</div>
								<!-- <div class="col-md-2"><img src=><?php echo $this->webroot.$ImageQuestions.$this->request->data['Questionpaper']['question']; ?>"</div> -->
							</div>
							
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/users'; ?>'">Cancel</button>
								</div>
							</div>
						</div>
						<?php echo $this->Form->end(); ?>
						<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>