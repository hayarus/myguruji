<!-- <div class="questions form">
<?php echo $this->Form->create('Question'); ?>
	<fieldset>
		<legend><?php echo __('Add Question'); ?></legend>
	<?php
		echo $this->Form->input('exampart');
		echo $this->Form->input('question');
		echo $this->Form->input('option_a');
		echo $this->Form->input('option_b');
		echo $this->Form->input('option_c');
		echo $this->Form->input('option_d');
		echo $this->Form->input('answer');
		echo $this->Form->input('explanation');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Questions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Results'), array('controller' => 'results', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Result'), array('controller' => 'results', 'action' => 'add')); ?> </li>
	</ul>
</div>
 -->

 <?php $this->Html->addCrumb('Questions', '/admin/questions'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<script type="text/javascript">

$(document).ready(function(){
var error1 = $('.alert-danger');
$('#QuestionAdminAddForm').validate({
errorElement: 'span', //default input error message container
errorClass: 'help-block', // default input error message classfalse
focusInvalid: false, // do not focus the last invalid input
ignore: "",
rules:{
"data[Question][question]" : {required : true },
"data[Question][exampart]" : {required : true },
"data[Question][option_a]" : {required : true},
"data[Question][option_b]" : { required : true},
"data[Question][option_c]" : {required : true},
"data[Question][option_d]" : { required : true},
"data[Question][answer]" : {required : true},
"data[Question][explanation]" : {required : true},
"data[Question][mark]" : {required : true},
},
messages:{
"data[Question][question]" : {required :"Please enter question."},
"data[Question][exampart]" : {required :"Please select exam part."},
"data[Question][option_a]" : {required :"Please enter  option a."},
"data[Question][option_b]" : {required :"Please enter option b."},
"data[Question][option_c]" : {required :"Please enter  option c."},
"data[Question][option_d]" : {required :"Please enter option d."},
"data[Question][answer]" : { required :"Please enter an answer."},
"data[Question][explanation]" : { required :"Please enter an explanation."},
"data[Question][mark]" : {required :"Please enter mark"}
},

invalidHandler: function (event, validator) { //display error alert on form submit              
//success1.hide();
error1.show();
//App.scrollTo(error1, -200);
},

highlight: function (element) { // hightlight error inputs
$(element)
.closest('.form-group').addClass('has-error'); // set error class to the control group
},

unhighlight: function (element) { // revert the change done by hightlight
$(element)
.closest('.form-group').removeClass('has-error'); // set error class to the control group
},

success: function (label) {
label
.closest('.form-group').removeClass('has-error'); // set success class to the control group
label
.closest('.form-group').removeClass('error');
},
});
});

</script>

<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i><?php echo __('Add Question'); ?>                        
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body form users">
						<div class="alert alert-danger display-hide">
						<button data-close="alert" class="close"></button>
						You have some form errors. Please check below.
						</div>

						<?php echo $this->Form->create('Question', array('class' => 'form-horizontal','method'=>'post')); ?>

						<div class="form-body">  
							<div class="form-group">
								<label class="col-md-3 control-label" for="">&nbsp;</label>
								<div class="col-md-4">
									<div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
								</div>
							</div>   
							<div class="form-group">
								<?php echo $this->Form->label('question<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('question', array('class' => 'form-control', 'type'=>'text', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<div class="form-group">
								<?php echo $this->Form->label('exam part<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('exampart', array('class' => 'form-control', 'label' => false, 'required' => false,'options' => $examparts));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('option a<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('option_a', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<div class="form-group">
								<?php echo $this->Form->label('option b<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('option_b', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<div class="form-group">
								<?php echo $this->Form->label('option c<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('option_c', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<div class="form-group">
								<?php echo $this->Form->label('option d<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('option_d', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<div class="form-group">
								<?php echo $this->Form->label('answer<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('answer', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<div class="form-group">
								<?php echo $this->Form->label('explanation<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('explanation', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<div class="form-group">
								<?php echo $this->Form->label('mark<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('mark', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/questions'; ?>'">Cancel</button>
								</div>
							</div>
						</div>
						
						<?php echo $this->Form->end(); ?>
					<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
