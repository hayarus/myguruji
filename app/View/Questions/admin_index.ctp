<!-- <div class="questions index">
	<h2><?php echo __('Questions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('exampart'); ?></th>
			<th><?php echo $this->Paginator->sort('question'); ?></th>
			<th><?php echo $this->Paginator->sort('option_a'); ?></th>
			<th><?php echo $this->Paginator->sort('option_b'); ?></th>
			<th><?php echo $this->Paginator->sort('option_c'); ?></th>
			<th><?php echo $this->Paginator->sort('option_d'); ?></th>
			<th><?php echo $this->Paginator->sort('answer'); ?></th>
			<th><?php echo $this->Paginator->sort('explanation'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($questions as $question): ?>
	<tr>
		<td><?php echo h($question['Question']['id']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['exampart']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['question']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['option_a']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['option_b']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['option_c']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['option_d']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['answer']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['explanation']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['created']); ?>&nbsp;</td>
		<td><?php echo h($question['Question']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $question['Question']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $question['Question']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $question['Question']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $question['Question']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Question'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Results'), array('controller' => 'results', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Result'), array('controller' => 'results', 'action' => 'add')); ?> </li>
	</ul>
</div>
 -->
 <?php $this->Html->addCrumb('Questions', '/admin/questions'); $paginationVariables = $this->Paginator->params(); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>


<!-- <?php $paginationVariables = $this->Paginator->params();?> -->

<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('Questions'); ?>                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
                <div class="table-toolbar">
                    <div class="btn-group"> 
                        <?php echo $this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('Questions'), 'action' => 'admin_index', 'admin' => true))); ?>
                            <div class="row">
                            <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <!-- <div class="input-group input-medium" >
                                        <input type="text" class="form-control" placeholder="Search here" name="search">
                                        <span class="input-group-btn">
	                                        <button class="btn green" type="button" onclick="this.form.submit()">Search <i class="fa fa-search"></i></button>
                                        </span>
                                    </div> -->
                                <!-- /input-group -->
                                </div>
                            <!-- /.col-md-6 -->
                            </div>
                        <!-- /.row -->
                        </form>
                    </div>
                    <div class="btn-group pull-right">
                        <div class="btn-group" style="padding-right:15px;">
	                        <?php echo $this->Html->link(__('New Question <i class="fa fa-plus"></i>'), array('action' => 'admin_add'), array('class' => 'btn green','escape' => FALSE)); ?> 
                        </div>
                    </div>
                </div>
                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <th>Question</th>
                        <!-- <th>Exam Part</th> -->
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                  </thead>
                  <?php  if(isset($questions) && sizeof($questions)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($questions as $question): $slno++?>
                  <tr>
                     <td><?php echo $slno+$paginationVariables['limit']*($paginationVariables['page']-1); ?></td>
                     <td><?php echo h($question['Question']['question']); ?>&nbsp;</td>
                     <!-- <td><?php echo h($question['Question']['exampart']); ?>&nbsp;</td> -->
                     <td>
					 <?php echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('action' => 'admin_edit', $question['Question']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>
					</td>
					<td>
					<?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'admin_delete', $question['Question']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
					</td>
					</tr>
					<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='7' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
              </div>
                <div class="row">
                <div class="col-md-7 col-sm-12">
                  <div class="dataTables_paginate paging_bootstrap">
                    <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                    <ul class="pagination" style="visibility: visible;">
                        <li class="prev disabled">
                        			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>

                        </li>
                        <li>
                        			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                        </li>
                        <li class="next disabled">
                        			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                        </li>
                    </ul>
                    <?php }?>
    				</div>
                    </div> 
    			</div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>


