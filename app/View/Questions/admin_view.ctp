<div class="questions view">
<h2><?php echo __('Question'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($question['Question']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exampart'); ?></dt>
		<dd>
			<?php echo h($question['Question']['exampart']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo h($question['Question']['question']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Option A'); ?></dt>
		<dd>
			<?php echo h($question['Question']['option_a']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Option B'); ?></dt>
		<dd>
			<?php echo h($question['Question']['option_b']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Option C'); ?></dt>
		<dd>
			<?php echo h($question['Question']['option_c']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Option D'); ?></dt>
		<dd>
			<?php echo h($question['Question']['option_d']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answer'); ?></dt>
		<dd>
			<?php echo h($question['Question']['answer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Explanation'); ?></dt>
		<dd>
			<?php echo h($question['Question']['explanation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($question['Question']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($question['Question']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Question'), array('action' => 'edit', $question['Question']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Question'), array('action' => 'delete', $question['Question']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $question['Question']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Results'), array('controller' => 'results', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Result'), array('controller' => 'results', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Results'); ?></h3>
	<?php if (!empty($question['Result'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Exampart'); ?></th>
		<th><?php echo __('Registration Id'); ?></th>
		<th><?php echo __('Exam Id'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th><?php echo __('Option'); ?></th>
		<th><?php echo __('Mark'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($question['Result'] as $result): ?>
		<tr>
			<td><?php echo $result['id']; ?></td>
			<td><?php echo $result['exampart']; ?></td>
			<td><?php echo $result['registration_id']; ?></td>
			<td><?php echo $result['exam_id']; ?></td>
			<td><?php echo $result['question_id']; ?></td>
			<td><?php echo $result['option']; ?></td>
			<td><?php echo $result['mark']; ?></td>
			<td><?php echo $result['status']; ?></td>
			<td><?php echo $result['created']; ?></td>
			<td><?php echo $result['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'results', 'action' => 'view', $result['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'results', 'action' => 'edit', $result['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'results', 'action' => 'delete', $result['id']), array('confirm' => __('Are you sure you want to delete # %s?', $result['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Result'), array('controller' => 'results', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
