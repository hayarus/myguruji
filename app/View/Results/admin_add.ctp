<!-- <div class="results form">
<?php echo $this->Form->create('Result'); ?>
	<fieldset>
		<legend><?php echo __('Add Result'); ?></legend>
	<?php
		echo $this->Form->input('exam_id');
		echo $this->Form->input('attented');
		echo $this->Form->input('wright');
		echo $this->Form->input('wrong');
		echo $this->Form->input('totalmark');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Results'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Exams'), array('controller' => 'exams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Exam'), array('controller' => 'exams', 'action' => 'add')); ?> </li>
	</ul>
</div>
 -->
  <?php $this->Html->addCrumb('Results', '/admin/results'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<script type="text/javascript">

$(document).ready(function(){
var error1 = $('.alert-danger');
$('#UserAdminAddForm').validate({
errorElement: 'span', //default input error message container
errorClass: 'help-block', // default input error message classfalse
focusInvalid: false, // do not focus the last invalid input
ignore: "",
rules:{
"data[Result][user_id]" : {required : true },
"data[Result][exam_id]" : {required : true},
"data[Result][attented]" : {required : true},
"data[Result][wright]" : {required : true},
"data[Result][wrong]" : {required : true},
"data[Result][totalmark]" : {required : true},
},
messages:{
"data[Result][user_id]" : {required :"Please select name."},
"data[Result][exam_id]" : {required :"Please select reg no."},
"data[Result][attented]" : {required :"Please enter password."},
"data[Result][wright]" : {required :"Please enter wright question."},
"data[Result][wrong]" : {required :"Please enter wrong  question."},
"data[Result][totalmark]" : {required :"Please enter totalmark."},
},

invalidHandler: function (event, validator) { //display error alert on form submit              
//success1.hide();
error1.show();
//App.scrollTo(error1, -200);
},

highlight: function (element) { // hightlight error inputs
$(element)
.closest('.form-group').addClass('has-error'); // set error class to the control group
},

unhighlight: function (element) { // revert the change done by hightlight
$(element)
.closest('.form-group').removeClass('has-error'); // set error class to the control group
},

success: function (label) {
label
.closest('.form-group').removeClass('has-error'); // set success class to the control group
label
.closest('.form-group').removeClass('error');
},
});
});

</script>

<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i><?php echo __('Add User'); ?>                        
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body form users">
						<div class="alert alert-danger display-hide">
						<button data-close="alert" class="close"></button>
						You have some form errors. Please check below.
						</div>

						<?php echo $this->Form->create('Result', array('class' => 'form-horizontal','method'=>'post')); ?>

						<div class="form-body">  
							<div class="form-group">
								<label class="col-md-3 control-label" for="">&nbsp;</label>
								<div class="col-md-4">
									<div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
								</div>
							</div>   
							<div class="form-group">
								<?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('user_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div> 
							<div class="form-group">
								<?php echo $this->Form->label('exam<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('exam_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('attented<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('attented', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('wright<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('wright', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('wrong<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('wrong', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('totalmark<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('totalmark', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div>
							<!-- <div class="form-group">
								<?php echo $this->Form->label('status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => false,'options'=>$boolenStatus));?>
								</div>
							</div>  -->
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/results'; ?>'">Cancel</button>
								</div>
							</div>
						</div>
						
						<?php echo $this->Form->end(); ?>
					<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
