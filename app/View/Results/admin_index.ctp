<!-- <div class="results index">
	<h2><?php echo __('Results'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('exam_id'); ?></th>
			<th><?php echo $this->Paginator->sort('attented'); ?></th>
			<th><?php echo $this->Paginator->sort('wright'); ?></th>
			<th><?php echo $this->Paginator->sort('wrong'); ?></th>
			<th><?php echo $this->Paginator->sort('totalmark'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($results as $result): ?>
	<tr>
		<td><?php echo h($result['Result']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($result['Exam']['id'], array('controller' => 'exams', 'action' => 'view', $result['Exam']['id'])); ?>
		</td>
		<td><?php echo h($result['Result']['attented']); ?>&nbsp;</td>
		<td><?php echo h($result['Result']['wright']); ?>&nbsp;</td>
		<td><?php echo h($result['Result']['wrong']); ?>&nbsp;</td>
		<td><?php echo h($result['Result']['totalmark']); ?>&nbsp;</td>
		<td><?php echo h($result['Result']['created']); ?>&nbsp;</td>
		<td><?php echo h($result['Result']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $result['Result']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $result['Result']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $result['Result']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $result['Result']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Result'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Exams'), array('controller' => 'exams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Exam'), array('controller' => 'exams', 'action' => 'add')); ?> </li>
	</ul>
</div>
 -->
 <?php $this->Html->addCrumb('Results', '/admin/results'); $paginationVariables = $this->Paginator->params(); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>


<!-- <?php $paginationVariables = $this->Paginator->params();?> -->

<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('Results'); ?>                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
                <div class="table-toolbar">
                    <div class="btn-group"> 
                        <?php echo $this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('Results'), 'action' => 'admin_index', 'admin' => true))); ?>
                            <div class="row">
                            <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <!-- <div class="input-group input-medium" >
                                        <input type="text" class="form-control" placeholder="Search here" name="search">
                                        <span class="input-group-btn">
	                                        <button class="btn green" type="button" onclick="this.form.submit()">Search <i class="fa fa-search"></i></button>
                                        </span>
                                    </div> -->
                                <!-- /input-group -->
                                </div>
                            <!-- /.col-md-6 -->
                            </div>
                        <!-- /.row -->
                        </form>
                    </div>
                    <div class="btn-group pull-right">
                        <div class="btn-group" style="padding-right:15px;">
	                        <!-- <?php echo $this->Html->link(__('New Result <i class="fa fa-plus"></i>'), array('action' => 'admin_add'), array('class' => 'btn green','escape' => FALSE)); ?>  -->
                        </div>
                    </div>
                </div>
                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <th>Reg No</th>
                        <th>Name</th>
                        <th>Mark</th>
                        <th>View</th>
                        <!-- <th>Delete</th> -->
                      </tr>
                  </thead>
                  <?php  if(isset($results) && sizeof($results)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($results as $result): $slno++?>
                  <tr>
                     <td><?php echo $slno+$paginationVariables['limit']*($paginationVariables['page']-1); ?></td>
                     
        						
                      <td><?php echo h($result['Result']['exam_id']); ?>&nbsp;</td>
                      <td><?php echo h($result['User']['name']); ?>&nbsp;</td>
                      <td><?php echo h($result['Result']['totalmark']); ?>&nbsp;</td>
                      
                     <td>
					 <?php echo $this->Html->link(__('<i class="fa fa-eye"></i> View'), array('action' => 'admin_view', $result['Result']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>
											</td>
											<!-- <td>
													<?php echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('action' => 'admin_edit', $result['Result']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>
											</td>
											<td>
													<?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'admin_delete', $result['Result']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
											</td> -->
										</tr>
									<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='7' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
              </div>
                <div class="row">
                        <div class="col-md-7 col-sm-12">
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                      <?php }?>
    						        </div>
                      </div> 
    				    </div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>


