<!-- <div class="results view">
<h2><?php echo __('Result'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($result['Result']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exam'); ?></dt>
		<dd>
			<?php echo $this->Html->link($result['Exam']['id'], array('controller' => 'exams', 'action' => 'view', $result['Exam']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Attented'); ?></dt>
		<dd>
			<?php echo h($result['Result']['attented']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Wright'); ?></dt>
		<dd>
			<?php echo h($result['Result']['wright']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Wrong'); ?></dt>
		<dd>
			<?php echo h($result['Result']['wrong']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Totalmark'); ?></dt>
		<dd>
			<?php echo h($result['Result']['totalmark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($result['Result']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($result['Result']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Result'), array('action' => 'edit', $result['Result']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Result'), array('action' => 'delete', $result['Result']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $result['Result']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Results'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Result'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Exams'), array('controller' => 'exams', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Exam'), array('controller' => 'exams', 'action' => 'add')); ?> </li>
	</ul>
</div>
 -->
 <?php $this->Html->addCrumb('Results', '/admin/results'); ?>
<?php $this->Html->addCrumb('View', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>



<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('View Work Sheet Exam Result'); ?> 
                        </div>
	                        <div class="tools">
	                            <a href="javascript:;" class="collapse"></a>
	                        </div>
	                    </div>
                    	<div class="portlet-body form universities">
		                    <div class="alert alert-danger display-hide">
		                        <button data-close="alert" class="close"></button>
		                        You have some form errors. Please check below.
		                    </div>
		                    <!-- BEGIN FORM-->
		                    <?php echo $this->Form->create('Result', array('class' => 'form-horizontal')); ?>
		                    <div class="form-body">                      



								<div class='form-group'>
									<?php echo $this->Form->label('' ,null, array('class' => 'col-md-3 control-label')); ?>
									<div class='col-md-4'>
									<p class="form-control-static"> </p>
									</div>
								</div>	


								
								 <table class="table table-striped table-bordered ">
								 	
								 	<tbody>
									<tr>
										<td><?php echo $this->Form->label('Applicant' ,null, array('class' => 'col-md-3 control-label')); ?></td>
										<td><p class="form-control-static"><?php echo h($result['User']['name']);?></p></td>
									</tr>
									<tr>
										<td><?php echo $this->Form->label('Regitration No' ,null, array('class' => 'col-md-3 control-label')); ?></td>
										<td><p class="form-control-static"><?php echo h($result['Result']['exam_id']);?></p></td>
									</tr>
									<tr>
										<td><?php echo $this->Form->label('Exam Date' ,null, array('class' => 'col-md-3 control-label')); ?></td>
										<td><p class="form-control-static"><?php echo h($result['Exam']['date']);?></p></td>
									</tr>
									<tr>
										<td><?php echo $this->Form->label('Attented Questions' ,null, array('class' => 'col-md-3 control-label')); ?></td>
										<td><p class="form-control-static"><?php echo h($result['Result']['attented']);?></p></td>
									</tr>
									<tr>
										<td><?php echo $this->Form->label('Corrected Answers' ,null, array('class' => 'col-md-3 control-label')); ?></td>
										<td><p class="form-control-static"><?php echo h($result['Result']['wright']);?></p></td>
									</tr>
									<tr>
										<td><?php echo $this->Form->label('Wrong Answers' ,null, array('class' => 'col-md-3 control-label')); ?></td>
										<td><p class="form-control-static"><?php echo h($result['Result']['wrong']);?></p></td>
									</tr>
									<tr>
										<td><?php echo $this->Form->label('Total Mark' ,null, array('class' => 'col-md-3 control-label')); ?></td>
										<td><p class="form-control-static"><?php echo h($result['Result']['totalmark']);?></p></td>
									</tr>
								</tbody>
								</table>
								

								
		                            <div class="form-actions fluid">
		                                <div class="col-md-offset-3 col-md-9">
		                                    <button type="button" class="btn blue" onclick="window.location='<?php echo $this->webroot.'admin/results'; ?>'">Go Back</button>
		                                </div>
		                            </div>
		                        </div>
		                        </div>
		                        <?php echo $this->Form->end(); ?>
		                        <!-- END FORM-->
		                    </div>
                	</div>
            </div>
        </div>
    </div>
</div>