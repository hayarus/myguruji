<!-- <div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('email');
		echo $this->Form->input('password');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('User.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Chatrooms'), array('controller' => 'chatrooms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Chatroom'), array('controller' => 'chatrooms', 'action' => 'add')); ?> </li>
	</ul>
</div>
 -->

<?php $this->Html->addCrumb('User', '/admin/users'); ?>
<?php $this->Html->addCrumb('Edit', ''); ?>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#UserAdminEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[User][name]" : {required : true },
		"data[User][email]" : {required : true},
		"data[User][password]" : {required : true},
		"data[User][group_id]" : {required : true},
		"data[User][status]" : {required : true},
		},
		messages:{
		"data[User][name]" : {required :"Please enter name."},
		"data[User][email]" : {required :"Please enter email."},
		"data[User][password]" : {required :"Please enter password."},
		"data[User][gruop_id]" : {required :"Please select group."},
		"data[User][status]" : {required :"Please select status."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
		//success1.hide();
		error1.show();
		//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
		$(element)
		.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
		$(element)
		.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
		label
		.closest('.form-group').removeClass('has-error'); // set success class to the control group
		label
		.closest('.form-group').removeClass('error');
		},
	});
});

</script>

<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box green">
				<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-reorder"></i><?php echo __('Edit User'); ?>                       
				</div>
				<div class="tools">
				<a href="javascript:;" class="collapse"></a>
				</div>
				</div>
					<div class="portlet-body form users">
						<div class="alert alert-danger display-hide">
						<button data-close="alert" class="close"></button>
						You have some form errors. Please check below.
						</div>
						<!-- BEGIN FORM-->
						<?php echo $this->Form->create('User', array('class' => 'form-horizontal' ,'method'=>'post')); ?>
						<div class="form-body">  
							<div class="form-group">
								<label class="col-md-3 control-label" for="">&nbsp;</label>
								<div class="col-md-4">
									<div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
								</div>
							</div>                                
							<div class="form-group">
								<!--<?php echo $this->Form->label('id<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?> -->
								<div class='col-md-4'>
									<?php echo $this->Form->input('id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
								</div>
							</div>
							
							<div class="form-group">
								<?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'required' => true));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('email<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false,'type'=>'email', 'required' => true));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('group<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('group_id', array('class' => 'form-control', 'type'=>'password', 'label' => false, 'required' => false));?>
								</div>
							</div>
							<div class="form-group">
								<?php echo $this->Form->label('status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
								<div class='col-md-4'>
									<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => false,'options'=>$boolenStatus));?>
								</div>
							</div> 
							
							<div class="form-actions fluid">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn blue">Submit</button>
									<button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/users'; ?>'">Cancel</button>
								</div>
							</div>
						</div>
						<?php echo $this->Form->end(); ?>
						<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>