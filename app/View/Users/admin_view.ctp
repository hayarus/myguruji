<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $user['User']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Chatrooms'), array('controller' => 'chatrooms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Chatroom'), array('controller' => 'chatrooms', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Chatrooms'); ?></h3>
	<?php if (!empty($user['Chatroom'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Replyparentid'); ?></th>
		<th><?php echo __('Comment'); ?></th>
		<th><?php echo __('Image'); ?></th>
		<th><?php echo __('Video'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['Chatroom'] as $chatroom): ?>
		<tr>
			<td><?php echo $chatroom['id']; ?></td>
			<td><?php echo $chatroom['user_id']; ?></td>
			<td><?php echo $chatroom['replyparentid']; ?></td>
			<td><?php echo $chatroom['comment']; ?></td>
			<td><?php echo $chatroom['image']; ?></td>
			<td><?php echo $chatroom['video']; ?></td>
			<td><?php echo $chatroom['created']; ?></td>
			<td><?php echo $chatroom['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'chatrooms', 'action' => 'view', $chatroom['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'chatrooms', 'action' => 'edit', $chatroom['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'chatrooms', 'action' => 'delete', $chatroom['id']), array('confirm' => __('Are you sure you want to delete # %s?', $chatroom['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Chatroom'), array('controller' => 'chatrooms', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
