$(document).ready(function(){
		$('.news-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 5000,
			arrows: true,
			prevArrow: $('.prev'),
			nextArrow: $('.next'),
			dots: false,
			pauseOnHover: false,
			responsive: [{
				breakpoint: 768,
				settings: {
				slidesToShow: 1
				}
				}, {
				breakpoint: 520,
				settings: {
				slidesToShow: 1
				}
			}]
		});

		$('.sliders').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			pauseOnHover: false,
			autoplay: true,
			infinite: true,
			cssEase: 'linear'
			});
		});
