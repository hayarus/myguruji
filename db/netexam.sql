-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2018 at 07:28 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `netexam`
--

-- --------------------------------------------------------

--
-- Table structure for table `nt_examquestions`
--

CREATE TABLE `nt_examquestions` (
  `id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` int(11) NOT NULL,
  `mark` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nt_examquestions`
--

INSERT INTO `nt_examquestions` (`id`, `exam_id`, `user_id`, `question_id`, `answer`, `mark`, `status`, `created`, `modified`) VALUES
(5, 6, 1, 1, 1, 2, 1, '2018-12-09 07:37:38', '2018-12-09 07:37:38'),
(6, 6, 1, 1, 1, 2, 1, '2018-12-09 07:38:47', '2018-12-09 07:38:47');

-- --------------------------------------------------------

--
-- Table structure for table `nt_exams`
--

CREATE TABLE `nt_exams` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `exampart` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nt_exams`
--

INSERT INTO `nt_exams` (`id`, `user_id`, `date`, `exampart`, `status`, `created`, `modified`) VALUES
(6, 1, '2018-12-10', 2, 1, '2018-12-09 06:06:44', '2018-12-09 06:06:44');

-- --------------------------------------------------------

--
-- Table structure for table `nt_groups`
--

CREATE TABLE `nt_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nt_groups`
--

INSERT INTO `nt_groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Administrators', '2018-12-11 07:07:51', '2018-12-11 07:27:35'),
(2, 'Students', '2018-12-11 07:10:39', '2018-12-11 07:10:39');

-- --------------------------------------------------------

--
-- Table structure for table `nt_newses`
--

CREATE TABLE `nt_newses` (
  `id` int(20) NOT NULL,
  `heading` varchar(300) NOT NULL,
  `dates` date NOT NULL,
  `details` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `status` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nt_newses`
--

INSERT INTO `nt_newses` (`id`, `heading`, `dates`, `details`, `image`, `status`, `created`, `modified`) VALUES
(1, 'xx', '2018-12-11', 'asa', 'img-188293-Chrysanthemum.jpg', 1, '2018-12-07 09:09:57', '2018-12-07 09:09:57'),
(2, 'xx', '2018-12-12', 'mmm', 'img-7861328-Desert.jpg', 1, '2018-12-07 09:15:33', '2018-12-07 09:15:46');

-- --------------------------------------------------------

--
-- Table structure for table `nt_questionpapers`
--

CREATE TABLE `nt_questionpapers` (
  `id` int(20) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `question` varchar(200) NOT NULL,
  `answerkey` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nt_questions`
--

CREATE TABLE `nt_questions` (
  `id` int(10) NOT NULL,
  `exampart` varchar(10) NOT NULL,
  `question` varchar(300) NOT NULL,
  `option_a` varchar(50) NOT NULL,
  `option_b` varchar(50) NOT NULL,
  `option_c` varchar(50) NOT NULL,
  `option_d` varchar(50) NOT NULL,
  `answer` varchar(100) NOT NULL,
  `explanation` varchar(100) NOT NULL,
  `mark` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nt_questions`
--

INSERT INTO `nt_questions` (`id`, `exampart`, `question`, `option_a`, `option_b`, `option_c`, `option_d`, `answer`, `explanation`, `mark`, `created`, `modified`) VALUES
(1, '1', 'm', 'n', 'n', 'm', 'j', 'm', 'm', 2, '2018-12-08 16:45:36', '2018-12-08 17:03:34');

-- --------------------------------------------------------

--
-- Table structure for table `nt_results`
--

CREATE TABLE `nt_results` (
  `id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `attented` int(11) NOT NULL,
  `wright` int(11) NOT NULL,
  `wrong` int(11) NOT NULL,
  `totalmark` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nt_results`
--

INSERT INTO `nt_results` (`id`, `exam_id`, `user_id`, `attented`, `wright`, `wrong`, `totalmark`, `created`, `modified`) VALUES
(4, 6, 1, 10, 8, 2, 20, '2018-12-09 11:08:39', '2018-12-09 11:08:39');

-- --------------------------------------------------------

--
-- Table structure for table `nt_subscriptions`
--

CREATE TABLE `nt_subscriptions` (
  `id` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `details` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nt_subscriptions`
--

INSERT INTO `nt_subscriptions` (`id`, `amount`, `details`, `created`, `modified`) VALUES
(2, '1000.00', 'for each module maximum four module can subscribe', '2018-12-08 15:56:07', '2018-12-08 15:56:07'),
(3, '7000.00', 'all module can subscribe', '2018-12-08 15:56:22', '2018-12-08 15:57:40');

-- --------------------------------------------------------

--
-- Table structure for table `nt_users`
--

CREATE TABLE `nt_users` (
  `id` int(10) NOT NULL,
  `group_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `subscription_id` int(10) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nt_users`
--

INSERT INTO `nt_users` (`id`, `group_id`, `name`, `email`, `password`, `subscription_id`, `status`, `created`, `modified`) VALUES
(1, 0, 'admin', 'admin@admin.com', 'f13729e3042aabb22c57a03b26de332aa1bb93a8', 2, 1, '2018-12-07 06:35:01', '2018-12-07 06:35:01'),
(2, 1, 'hr', 'hr@hr.com', 'ttls123', 0, 1, '2018-12-11 07:26:05', '2018-12-11 07:26:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nt_examquestions`
--
ALTER TABLE `nt_examquestions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `exam_id` (`exam_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `nt_exams`
--
ALTER TABLE `nt_exams`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `nt_exams_ibfk_1` (`user_id`);

--
-- Indexes for table `nt_groups`
--
ALTER TABLE `nt_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nt_newses`
--
ALTER TABLE `nt_newses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nt_questionpapers`
--
ALTER TABLE `nt_questionpapers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nt_questions`
--
ALTER TABLE `nt_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nt_results`
--
ALTER TABLE `nt_results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exam_id` (`exam_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `nt_subscriptions`
--
ALTER TABLE `nt_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nt_users`
--
ALTER TABLE `nt_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nt_examquestions`
--
ALTER TABLE `nt_examquestions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `nt_exams`
--
ALTER TABLE `nt_exams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `nt_groups`
--
ALTER TABLE `nt_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nt_newses`
--
ALTER TABLE `nt_newses`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nt_questionpapers`
--
ALTER TABLE `nt_questionpapers`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nt_questions`
--
ALTER TABLE `nt_questions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nt_results`
--
ALTER TABLE `nt_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `nt_subscriptions`
--
ALTER TABLE `nt_subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nt_users`
--
ALTER TABLE `nt_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `nt_examquestions`
--
ALTER TABLE `nt_examquestions`
  ADD CONSTRAINT `nt_examquestions_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `nt_questions` (`id`),
  ADD CONSTRAINT `nt_examquestions_ibfk_3` FOREIGN KEY (`exam_id`) REFERENCES `nt_exams` (`id`),
  ADD CONSTRAINT `nt_examquestions_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `nt_users` (`id`);

--
-- Constraints for table `nt_exams`
--
ALTER TABLE `nt_exams`
  ADD CONSTRAINT `nt_exams_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `nt_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `nt_results`
--
ALTER TABLE `nt_results`
  ADD CONSTRAINT `nt_results_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `nt_exams` (`id`),
  ADD CONSTRAINT `nt_results_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `nt_users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
